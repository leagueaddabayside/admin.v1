
'use strict';
angular.module('adminFantasyApp')
        .filter('dateFormat', function ($filter) {
            return function (input) {  
               var x = new Date(input);
               x = $filter('date')(input, "dd-MM-yyyy hh:mm:ss");
                return x;
            };
        });


