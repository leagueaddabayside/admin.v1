'use strict';
angular.module('adminFantasyApp')
        .controller('tourMatchesCtrl', function ($scope, ngDialog, UtilityService, msgConstants, serverApi, myConfig) {
            var that = this;
            that.matchlist = [];
            that.showAlert = false;
            that.isDataError = false;
            that.showForm = true;
            that.alertText = "";
            that.tourList = [];
            that.selected = '';
            that.miniFormClass = '';

            var getTourAndMatchData = function () {
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response) {
                    if (response.respCode == 100) {
                        angular.forEach(response.respData, function (value, key) {
                            if (value.status != 'PENDING')
                                that.tourList.push(value);
                        });
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                    

                });
            };
            getTourAndMatchData();

            // To Get Match List of Selected Tour.
            that.getMatchList = function () {
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                that.miniFormClass = 'minimize-form-anim';
                var data = {};
                if (that.selected.tourId == '' || that.selected.tourId == 'undefined') {
                    return;
                } else {
                    data = {
                        tourId: that.selected.tourId,
                    }
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_LIST, data, function (response) {
                    if (response.respCode == 100) {
                        that.matchlist = response.respData;
                        that.showForm = false;
                    }
                    else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            }

            // Get Match Data to edit.
            that.getMatchData = function (matchId) {
                var data = {};
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                that.isDataError = false;
                that.alertText = "";
                data = {
                    matchId: matchId,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_INFO, data, function (response) {
                    if (response.respCode === 100) {
                        that.matchData = response.respData;
                    }
                    else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            }


            // Submit Match Data After Edit.
            that.onSubmitMatchData = function () {
                var data = {};
                that.errorData = msgConstants.ERR_FORM_DATA;
                that.message = '';
                that.messageClass = '';
                that.showAlert = false;
                var isValid = true;
                data = {
                    matchId: that.matchData.matchId,
                    name: that.matchData.name,
                    relatedName: that.matchData.relatedName,
                    status: that.matchData.status,
                    shortName : that.matchData.shortName,
                }
                if (that.matchData.status == '') {
                    that.isDataError = true;
                    that.alertText = msgConstants.ERR_POPUP_STATUS_BLANK;
                    isValid = false;
                    return;
                }
                if (that.matchData.relatedName == '') {
                    that.isDataError = true;
                    that.alertText = "Related Name " + msgConstants.ERR_FIELD_BLANK;
                    isValid = false;
                    return;
                }
                if (that.matchData.shortName == '') {
                    that.isDataError = true;
                    that.alertText = "Short Name " + msgConstants.ERR_FIELD_BLANK;
                    isValid = false;
                    return;
                }
                if (isValid) {
                    if (that.matchData.status == 'CLOSED') {
                        UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_CLOSE_DATA_UPDATE, data, function (response)  {
                            ngDialog.close();
                            if (response.respCode == 100) {
                                that.showAlert = true;
                                that.messageClass = 'alert-success';
                                that.message = "Match " + msgConstants.SUCCESS_SERVER_RESP_UPDATE;
                                that.getTourData();
                                return;
                            } else {
                                that.showAlert = true;
                                that.message = response.message;
                                that.messageClass = 'alert-danger';
                            }
                        });
                    } else {
                        UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_DATA_UPDATE, data, function (response)  { 
                            ngDialog.close();
                            if (response.respCode == 100) {
                                that.showAlert = true;
                                that.messageClass = 'alert-success';
                                that.message = "Match " + msgConstants.SUCCESS_SERVER_RESP_UPDATE;
                                that.getTourData();
                                return;
                            } else {
                                that.showAlert = true;
                                that.message = response.message;
                                that.messageClass = 'alert-danger';
                            }
                        });
                    }
                }
            }

            that.onCloseAlert = function () {
                that.showAlert = false;
            };

            that.getTime = function (time) {
                time = time * 1000;
                time = new Date(time);
                return time.toDateString();
            }

            that.getTourData = function () {
                if (that.selected == undefined) {
                    getTourAndMatchData();
                } else {
                    that.getMatchList();
                }
            }

            that.showHideFormFields = function (data) {
                if (data == 'hide-form') {
                    that.showForm = false;
                }
                if (data == 'show-form') {
                    that.showForm = true;
                }
            }

            moment.locale('en');
        });
