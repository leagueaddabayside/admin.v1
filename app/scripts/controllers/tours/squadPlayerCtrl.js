'use strict';
angular.module('adminFantasyApp')
        .controller('squadPlayerCtrl', function ($scope, ngDialog, UtilityService, msgConstants, myConfig, serverApi) {

            var that = this;
            that.showPlayerList = false;
            that.showAlert = false;
            that.showForm = true;
            that.alertText = "";
            that.showPlayerListErrorMsg = '';
            that.squadPlayerList = [];
            that.errorMsg = '';
            that.tourList = [];

            // Default Getting ALL Tour List.
            angular.element(document).ready(function () {
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response) {
                    if (response.respCode == 100) {
                        for (var i = 0; i < response.respData.length; i++) {
                            if (response.respData[i].status != 'PENDING') {
                                that.tourList.push(response.respData[i]);
                            }
                        }
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            });

            that.getTeamList = function () {
                that.teamId = '';
                that.teamList = that.selected.teams;
            }

            // Get Player List after Tour Selection.
            that.getPlayerListData = function () {
                var data = {};
                that.showAlert = false;
                that.errorMsg = "";
                that.showPlayerListErrorMsg = '';
                data = {
                    tourId: that.selected.tourId,
                }
                if (that.key != '') {
                    data.teamKey = that.key;
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_PLAYER_LIST, data, function (response) {
                    if (response.respCode == 100 && response.respData.length > 0) {
                        that.showForm = false;
                        that.showPlayerList = true;
                        that.squadPlayerList = response.respData;
                        that.playerList = that.squadPlayerList;
                        that.playerList.sortArray("points");
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                        that.showPlayerListErrorMsg = msgConstants.ERR_DNF;
                        that.showPlayerList = false;
                        that.playerType = 'all';
                    }
                });
            }

            // Get Player form fields to edit in pop-up.
            that.editPlayerDetail = function (tourId, playerId) {
                that.showAlert = false;
                that.alertText = "";
                that.errorMsg = '';
                var data = {};
                that.playerId = playerId;
                data = {
                    tourId: tourId,
                    playerId: that.playerId,
                    token: localStorage.adminToken,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.PLAYER_INFO, data, function (response) {
                    if (response.respCode === 100) {
                        that.playerData = response.respData;
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            }

            // Update player details.
            that.onSubmitPlayerData = function () {
                that.message = '';
                that.messageClass = '';
                that.showAlert = false;
                var isValid = true;
                that.errorMsg = "";
                if (that.playerData.bowler == false && that.playerData.keeper == false && that.playerData.batsman == false) {
                    isValid = false;
                    that.errorMsg = msgConstants.ERR_PLY_INFO_BLANK;
                    return;
                }
                if (that.playerData.credit < 7 || that.playerData.credit > 11.5) {
                    isValid = false;
                    that.errorMsg = msgConstants.ERR_CR_POINTS;
                    return;
                }
                if (that.playerData.status == '') {
                    isValid = false;
                    that.errorMsg = msgConstants.ERR_POPUP_STATUS_BLANK;
                    return;
                }
                if (isValid) {
                    var data = {
                        tourId: that.playerData.tourId,
                        playerId: that.playerId,
                        credit: that.playerData.credit,
                        status: that.playerData.status,
                        keeper: that.playerData.keeper,
                        batsman: that.playerData.batsman,
                        bowler: that.playerData.bowler,
                        ONE_DAY: that.playerData.ONE_DAY,
                        TEST: that.playerData.TEST,
                        T20: that.playerData.T20,
                    }
                    UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.UPDATE_PLAYER_DATA, data, function (response) {
                        that.getPlayerListData();
                        ngDialog.close();
                        if (response.respCode == 100) {
                            that.showAlert = true;
                            that.messageClass = 'alert-success';
                            that.message = msgConstants.SUCCESS_SERVER_RESP_UPDATE;
                            that.playerId = '';
                            return;
                        } else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            that.playerId = '';
                            return;
                        }
                    });
                }
            }

            that.getCategoryPlayer = function () {
                document.getElementById('infoPlayer').classList.remove("open");
                if (that.playerType == 'all') {
                    that.playerList = that.squadPlayerList;
                    return;
                }
                if (that.playerType == 'allrounder') {
                    that.playerList = [];
                    angular.forEach(that.squadPlayerList, function (value, key) {
                        if (that.squadPlayerList[key].batsman == true && that.squadPlayerList[key].bowler == true) {
                            that.playerList.push(that.squadPlayerList[key]);
                        }
                    });
                    return;
                }
                if (that.playerType == 'bowler') {
                    that.playerList = [];
                    angular.forEach(that.squadPlayerList, function (value, key) {
                        if (that.squadPlayerList[key].bowler == true && that.squadPlayerList[key].batsman == false) {
                            that.playerList.push(that.squadPlayerList[key]);
                        }
                    });
                    return;
                }
                if (that.playerType == 'batsman') {
                    that.playerList = [];
                    angular.forEach(that.squadPlayerList, function (value, key) {
                        if (that.squadPlayerList[key].batsman == true && that.squadPlayerList[key].bowler == false) {
                            that.playerList.push(that.squadPlayerList[key]);
                        }
                    });
                    return;
                } else {
                    that.playerList = [];
                    angular.forEach(that.squadPlayerList, function (value, key) {
                        if (that.squadPlayerList[key].keeper == true) {
                            that.playerList.push(that.squadPlayerList[key]);
                        }
                    });
                    return;
                }

                that.playerType = '';
            }

            that.onCloseAlert = function () {
                that.showAlert = false;
            };

            that.playerInfo = function (item) {
                // console.log("ITEM",item);
                if (item.keeper == true) {
                    return 'keeper';
                }
                if (item.batsman == true && item.bowler == true) {
                    return 'allrounder';
                }
                if (item.batsman == true) {
                    return 'batsman';
                } else {
                    return 'bowler';
                }
            }

            that.showHideFormFields = function (data) {
                if (data == 'hide-form') {
                    that.showForm = false;
                }
                if (data == 'show-form') {
                    that.showForm = true;
                }
            }



            moment.locale('en');
        });
