'use strict';
angular.module('adminFantasyApp')
        .controller('allToursListCtrl', function ($scope, ngDialog, UtilityService, $timeout, pagination, msgConstants, myConfig,serverApi ) {
            var that = this;
            that.allToursList =[];
            that.showAlert = false;   
            that.errorMsg = '';
            that.pageCount = [];
            that.pageSize = 10;
            that.allToursList = [];
            that.itemList = [];
            var num = 10;
            //Fetch All Tourd Data.-----
            var getchAllTourList = function () {
                var data = {}; 
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response) {
                    if (response.respCode == 100) {
                    that.allToursList = response.respData;
                    that.setPageSize();
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            }  
             angular.element(document).ready(function () {
                 getchAllTourList();
            })           

            //Click on Edit Link to Edit Tour Details.
            that.editTemplateData = function (id) {
                var data = {};
                var activeDate = '';
                that.errorMsg = '';
                that.errorData = "Errron In Form Data.";
                that.message = '';
                that.messageClass = 'alert-success';
                that.showAlert = false;  
                that.alertText = '';
                that.tourId = id;
                data.tourId =  that.tourId;
                that.tourData = [];
                that.pageIndex = 1;
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_DATA_FETCH, data, function (response) {
                    if (response.respCode === 100) {
                        that.tourData = response.respData;  
                    }
                    else{
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            }
           
            //On Submit Tour Data after Update.
            that.onSubmitFormData = function (tourId) {
                var data = {};
                that.message = '';
                that.showAlert = false;   
                that.errorMsg = '';
                var isValid = true;
                data = {
                    tourId: tourId,
                    status: that.tourData.status,
                    activeDate: that.tourData.activeDate,
                    shortName: that.tourData.shortName,
                    tourName: that.tourData.tourName,
                    token : localStorage.adminToken,
                }
                if(that.tourData.shortName == ''){
                   isValid = false;
                   that.errorMsg = "Short Name " + msgConstants.ERR_FIELD_BLANK; 
                   return;
                }
                if(that.tourData.status == ''){
                   isValid = false;
                   that.errorMsg = msgConstants.ERR_POPUP_STATUS_BLANK; 
                   return;
                }
                if (isValid) {
                    UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_DATA_UPDATE, data, function (response)  {
                        if (response.respCode == 100) {
                            ngDialog.close();
                            that.showAlert = true;
                            that.messageClass = 'alert-success';
                            that.message = msgConstants.SUCCESS_SERVER_RESP_UPDATE;
                            getchAllTourList();
                            return;
                        } else {
                            ngDialog.close();
                            that.showAlert = true;
                            that.message = msgConstants.ERR_SERVER;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                    });
                }
            }
            
            that.onCreateLeague = function(tourId){
                var data = {};
                that.message = '';
                that.showAlert = false;               
                data = {
                    tourId: tourId, 
                    token : localStorage.adminToken,
                }
                if (data.tourId !='') {
                    UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.CREATE_DEFAULT_LEAGUE, data, function (response)  { 
                        if (response.respCode == 100) {
                            ngDialog.close();
                            that.showAlert = true;
                            that.messageClass = 'alert-success';
                            that.message = msgConstants.CREATE_LEAGUE_SUCCESS;
                            getchAllTourList();
                            return;
                        } else {
                            ngDialog.close();
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                    });
                }
            }
            
            that.onCloseAlert = function(){
                that.showAlert = false;
            };
            
            that.setPageSize = function () {
                that.pageCount = [];
                var data = {
                    pageCount: that.pageCount,
                    listLength: that.allToursList.length,
                    pageSize: that.pageSize,
                }
                pagination.setPageSize(data, function (response) {
                    $timeout(function () {
                        that.getPageList(1);
                    }, 10);
                    that.pageCount = response.pageCount;
                    that.pageSize = response.pageSize;

                });
            }
            
            that.getPageList = function (pageNo) {
                that.activeClass = '';
                var data = {
                    pageNo : pageNo,
                    newPageListData : [],        
                    pageCount: that.pageCount,
                    recordData: that.allToursList,
                    pageSize: that.pageSize,
                }
                 pagination.getPageList(data, function (response) {
                     that.showForm = false;
                     that.activeClass = response.activeClass;
                     that.pageDataList = response.newPageListData;
                     that.pageIndex = pageNo-1;
                });                
            }
                       
//            that.getPageList(1);
            
            moment.locale('en');
        });
