'use strict';
angular.module('adminFantasyApp')
        .controller('DebitFromUserCtrl', function ($scope, UtilityService, $location, $rootScope, msgConstants, myConfig, serverApi) {
            var that = this;
            var defaultData = {remarks: '', secreenName : '', userId : '-1', depositAmt: '', bonusAmt: '', winningAmt: '', newAccBalance : '',
                newBonusAmt : '', newDepositAmt : '', newWinningAmt : '',
                totalWinning :'', totalBalance:'', userBalance:''};
            var defaultError = {userDataErrMsg : '', depositAmountErrMsg: '', winningAmountErrMsg: '', bonusAmountErrMsg: '', userRemarkErrMsg : ''};
            that.result = angular.copy(defaultData);
            that.errorData = angular.copy(defaultError);
            that.userName = '';
            that.currencyType = '`';
            that.showAlert = false;
            that.message = '';
            
            that.calcNewBalance = function(){
                that.result.newAccBalance = that.result.userBalance - (Number(that.result.newWinningAmt) + Number(that.result.newBonusAmt) + Number(that.result.newDepositAmt));               
                that.result.newAccBalance = Math.round( that.result.newAccBalance * 1e12 ) / 1e12 ;
            }
            
            that.getUserDetail = function () {
                that.showAlert = false;  
                that.result = angular.copy(defaultData);
                that.errorData = angular.copy(defaultError);
                if(that.userName == ''){
                    that.errorData.userDataErrMsg = msgConstants.ERR_USER_NAME_REQ;
                    return;
                }
                if (that.userName != '') {
                    var data = {
                        userData: that.userName,
                        token : localStorage.adminToken,
                    };                
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.USER_ACC_INFO, data, function (response)  {   
                        if (response.respCode == 100) {  
                            that.userAccInfo = response.respData.screenName + " has " + that.currencyType + " " + that.result.totalBalance;
                            that.result = response.respData;
                            that.result.userBalance = response.respData.depositAmt + response.respData.winningAmt + response.respData.bonusAmt;  
                            that.result.userBalance = Math.round( that.result.userBalance * 1e12 ) / 1e12 ;
                            that.result.totalBalance = response.respData.screenName + " has " + that.currencyType + " " + that.result.userBalance;                                   
                            that.result.newWinningAmt = '';
                            that.result.newBonusAmt =  '';
                            that.result.newDepositAmt =  '';
                            that.result.newAccBalance = that.userBalance;
                        }
                        else {
                            that.errorData.userDataErrMsg = response.message;
                        }
                    });
                }
                
            }
                      
            that.onSubmitDebittForm = function () {
                that.showAlert = false;
                that.errorData = angular.copy(defaultError);
                that.message = '';
                that.messageClass = 'alert-success';
                var errorData = that.errorData;
                var reqData = {};
                var isSuccess = true;
                if (that.result.userId == -1) {
                    reqData.userChips = '';
                    that.errorData.userDataErrMsg = msgConstants.ERR_USER_NAME_REQ;
                    isSuccess = false;
                    return;
                }
                if (that.result.newDepositAmt <0 || that.result.newDepositAmt > that.result.depositAmt) {
                    that.errorData.depositAmountErrMsg = msgConstants.ERR_DEPOSIT_INVALID;
                    isSuccess = false;
                    return;
                }
                if (that.result.newBonusAmt < 0 || that.result.newBonusAmt > that.result.bonusAmt ) {
                    that.errorData.bonusAmountErrMsg = msgConstants.ERR_BONUS_INVALID ;
                    isSuccess = false;
                    return;
                }
                if (that.result.newWinningAmt < 0 || that.result.newWinningAmt > that.result.winningAmt) {
                    that.errorData.winningAmountErrMsg = msgConstants.ERR_WINNING_INVALID;
                    isSuccess = false;
                    return;
                }                   
                if (that.result.newDepositAmt == 0 &&  that.result.newBonusAmt == 0 && that.result.newWinningAmt == 0) {                    
                    that.showAlert = true;
                    that.message = msgConstants.ERR_AMT_CANNOT_0;
                    that.messageClass = 'alert-danger';
                    isSuccess = false;
                    return;
                } 
                if (that.result.remarks == '' || that.result.remarks == undefined) {
                    that.errorData.userRemarkErrMsg = msgConstants.ERR_REMARKS_REQUIRE;
                    isSuccess = false;
                    return;
                }
                if (isSuccess) {
                    $rootScope.$emit('showloader');
                    var data = {
                        userId: that.result.userId,
                        newDepositAmt : that.result.newDepositAmt,
                        newBonusAmt : that.result.newBonusAmt,
                        newWinningAmt : that.result.newWinningAmt, 
                        remarks: that.result.remarks,
                        adminId : 1,
                        txnType : 'DEBIT',
                        token : localStorage.adminToken,
                        
                    };
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.USER_DR_UPDATE, data, function (response)  { 
                        if (response.respCode == 100) {
                            that.showAlert = true;
                            that.userName = '';
                            that.result = angular.copy(defaultData);
                            that.errorData = angular.copy(defaultError);
                            that.messageClass = 'alert-success';
                            that.message = msgConstants.CR_DR_TRANS_SUCCESS + " : " + response.respData;
                            return;
                        }                       
                        else {
                            that.userName = '';
                            that.showAlert = true;
                            that.result = angular.copy(defaultData);
                            that.errorData = angular.copy(defaultError);
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                        }
                    });
                }
            };

            that.onResetFormData = function () {                                                
                that.result.newWinningAmt = '';
                that.result.newBonusAmt =  '';
                that.result.newDepositAmt =  '';
                that.errorData = angular.copy(defaultError);
                that.result = angular.copy(defaultData);           
                that.message = '';
                that.showAlert = false;
                that.userName ='';
            };
            
            that.onCloseAlert = function(){
                that.showAlert = false;
            };
            return that;

        });
