'use strict';
angular.module('adminFantasyApp')
        .controller('changePasswordCtrl', function ($scope, UtilityService, $rootScope, $location, myConfig, serverApi) {
            var that = this;
            $rootScope.isMenuSelected = -1;
            var defaultData = {currentPassword: '', newPassword: '', confirmNewPassword: ''};
            var defaultErrorData = {userPasswordError: '', newPasswordError: '', confirmNewPassword: ''};
            that.result = angular.copy(defaultData);
            that.errorData = angular.copy(defaultErrorData);
            that.showAlert = false;
            that.onSubmitFormData = function () {
                that.errorData = angular.copy(defaultErrorData);
                that.message = "";
                that.messageClass = '';
                var isSuccess = true;
                if (that.result.currentPassword == '') {
                    isSuccess = false;
                    that.errorData.userPasswordError = 'Current Password cannot be black';
                }
                if (that.result.newPassword == '' && that.result.confirmNewPassword == '') {
                    isSuccess = false;
                    that.errorData.newPasswordError = 'Field cannot be left blank.';
                    that.errorData.confirmNewPassword = 'Field cannot be left blank.';
                }

                if (that.result.newPassword != that.result.confirmNewPassword) {
                    isSuccess = false;
                    that.errorData.confirmNewPassword = 'New Password & Confirm Password should be same.';
                }
                if (isSuccess) {
                    $rootScope.$emit('showloader');
                    var data = {};
                    that.result.currentPassword = md5(that.result.currentPassword);
                    that.result.newPassword = md5(that.result.newPassword);
                    data = {
                        userId: $rootScope.userId,
                        password : that.result.currentPassword,
                        newPassword: that.result.newPassword,
                    };
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CHANGE_PASS_REQ, data, function (response){
                        $rootScope.$emit('hideloader');
                        if (response.respCode == 100) {
                            that.showAlert = true;
                            that.message = "Password change successfully.";
                            that.messageClass = 'alert-success';
                            that.result = angular.copy(defaultData);
                            return;
                        }
                        else {
                            that.showAlert = true;
                            that.result = angular.copy(defaultData);
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                    });
                }

            };
            that.onResetFormData = function () {
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                that.result = angular.copy(defaultData);
                that.errorData = angular.copy(defaultErrorData);
            };
            that.onCloseAlert = function () {
                that.showAlert = false;
            };
        });
