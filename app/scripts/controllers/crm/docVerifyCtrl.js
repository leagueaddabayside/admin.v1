(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('docVerifyCtrl', function ($scope, ngDialog, UtilityService, msgConstants, myConfig, serverApi) {
                var that = this;
                that.showAlert = false;       
                that.pendingDocList = [];
                that.remark ='';
                that.errorMsg = '';
                that.message  = '';
                that.getPendingDocList = function (item) {
                    var data = {};
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.DOC_PENDING, data, function (response) {
                        if (response.respCode == 100) {
                            that.pendingDocList = response.respData;
                        } else {
                             that.showAlert = true;
                             that.message = response.message;
                             that.messageClass = 'alert-danger';
                             
                        }
                    });
                }

                that.getPendingDocList();

                that.onSubmitDocStatusData = function () {
                    that.message = '';
                    that.messageClass = '';
                    that.showAlert = false;

                    var data = {
                        userId: that.userDocDetail.userId,                       
                        status: that.userDocDetail.status,
                        remarks: that.remark,
                        token: localStorage.adminToken,
                    }
                    if(that.userDocDetail.status == ''){
                        that.errorMsg = msgConstants.ERR_POPUP_STATUS_BLANK;
                        return;
                    }
                    if(that.userDocDetail.status == 'FAILED' && that.remark == ''){
                        that.errorMsg = msgConstants.ERR_REMARKS_REQ_FAIL;
                        return;
                    }
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.DOC_VERIFY, data, function (response) {
                        if (response.respCode == 100) {
                            ngDialog.close();
                            that.message = msgConstants.DOC_VERIFY_SUCCESS + that.userDocDetail.status + ".";
                            that.messageClass = 'alert-success';
                            that.showAlert = true;
                            that.getPendingDocList();
                        } else {
                            ngDialog.close();
                            that.showAlert = true;
                            that.messageClass = 'alert-danger';
                            that.message = response.message;
                            that.getPendingDocList();
                        }
                    });
                }

                that.getReqDate = function (time) {
                    time = new Date(time);
                    return time.toDateString();
                }

                that.onCloseAlert = function () {
                    that.showAlert = false;
                };

                that.openDialog = function (docDetail) {
                    that.userDocDetail = '';
                    that.remark = '';
                    that.userDocDetail = docDetail; 
                }

                moment.locale('en');

            })

})();