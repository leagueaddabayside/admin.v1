'use strict';
angular.module('adminFantasyApp')
        .controller('CreditToUserCtrl', function ($scope, $location, $rootScope, msgConstants, UtilityService, myConfig, serverApi) {
            var that = this;
            var defaultData = {secreenName: '', userId: '-1', depositAmt: '', bonusAmt: '', winningAmt: '', newAccBalance: '',
                newBonusAmt: '', newDepositAmt: '', newWinningAmt: '',
                totalWinning: '', remarks: '', totalBalance: '', userBalance: ''};
            var defaultError = {userDataErrMsg: '', depositAmountErrMsg: '', winningAmountErrMsg: '', bonusAmountErrMsg: '', userRemarkErrMsg:''};
            that.result = angular.copy(defaultData);
            that.errorData = angular.copy(defaultError);
            that.userName = '';
            that.currencyType = '`';
            that.showAlert = false;
            that.message = '';

            that.calcNewBalance = function () {
                that.result.newAccBalance = that.result.userBalance + Number(that.result.newWinningAmt) + Number(that.result.newBonusAmt) + Number(that.result.newDepositAmt);
                that.result.newAccBalance = Math.round( that.result.newAccBalance * 1e12 ) / 1e12 ;
            }

            that.getUserDetail = function () {
                that.errorData = angular.copy(defaultError);
                that.result = angular.copy(defaultData);
                that.showAlert = false;
                var totalBalance = 0;
                if (that.userName == '') {
                    that.errorData.userDataErrMsg = msgConstants.ERR_USER_NAME_REQ;
                    return;
                }
                if (that.userName != '') {
                    var data = {
                        userData: that.userName,
                        token: localStorage.adminToken,
                    };
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.USER_ACC_INFO, data, function (response) {
                        if (response.respCode == 100) {
                            that.result = response.respData;
                            that.result.userBalance = response.respData.depositAmt + response.respData.winningAmt + response.respData.bonusAmt;
                            that.result.userBalance = Math.round( that.result.userBalance * 1e12 ) / 1e12 ;
                            that.result.totalBalance = response.respData.screenName + " has " + that.currencyType + " " + that.result.userBalance;
                            that.result.newWinningAmt = '';
                            that.result.newBonusAmt = '';
                            that.result.newDepositAmt = '';
                        } else {
                            that.errorData.userDataErrMsg = response.message;
                        }
                    });
                }

            }

            that.onSubmitCreditForm = function () {
                that.showAlert = false;
                that.errorData = angular.copy(defaultError);
                that.message = '';
                that.messageClass = 'alert-success';
                var errorData = that.errorData;
                var isSuccess = true;
                
                if (that.result.userId == -1) {
                    that.errorData.userDataErrMsg = msgConstants.ERR_USER_NAME_INVALID;
                    isSuccess = false;
                    return;
                }
                
                if (that.result.newDepositAmt < 0) {
                    that.errorData.depositAmountErrMsg = msgConstants.ERR_AMT_CANNOT_0;
                    isSuccess = false;
                    return;
                }
                if (that.result.newBonusAmt < 0) {
                    that.errorData.bonusAmountErrMsg = msgConstants.ERR_AMT_CANNOT_0;
                    isSuccess = false;
                    return;
                }
                if (that.result.newWinningAmt < 0) {
                    that.errorData.winningAmountErrMsg = msgConstants.ERR_AMT_CANNOT_0;
                    return;
                }
                if (that.result.newWinningAmt == 0 && that.result.newBonusAmt == 0 && that.result.newDepositAmt == 0) {
                    that.showAlert = true;
                    that.message = msgConstants.ERR_AMT_CANNOT_0;
                    that.messageClass = 'alert-danger';
                    isSuccess = false;
                    return;
                }
                if (that.result.newDepositAmt == that.result.depositAmt && that.result.newBonusAmt == that.result.bonusAmt && that.result.newWinningAmt == that.result.winningAmt) {
                    that.errorData.amountError = msgConstants.ERR_NOCHANGE_ACC_INFO;
                    isSuccess = false;
                    return;
                }
                if (that.result.remarks == '' || that.result.remarks == undefined ) {
                    that.errorData.userRemarkErrMsg = msgConstants.ERR_REMARKS_REQUIRE;
                    isSuccess = false;
                    return;
                }
                if (isSuccess) {
                    var data = {
                        userId: that.result.userId,
                        newDepositAmt: that.result.newDepositAmt,
                        newBonusAmt: that.result.newBonusAmt,
                        newWinningAmt: that.result.newWinningAmt,
                        remarks: that.result.remarks,
                        adminId: 1,
                        token: localStorage.adminToken,
                        txnType: 'CREDIT',
                    };
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.USER_CR_UPDATE, data, function (response) {
                        if (response.respCode == 100) {
                            that.showAlert = true;
                            that.userName = '';
                            that.result = angular.copy(defaultData);
                            that.errorData = angular.copy(defaultError);
                            that.messageClass = 'alert-success';
                            that.message = msgConstants.CR_DR_TRANS_SUCCESS + " " + response.respData;
                            return;
                        } else {
                            that.userName = '';
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            that.result = angular.copy(defaultData);
                            that.errorData = angular.copy(defaultError);
                        }
                    });
                }
            };

            that.onResetFormData = function () {
                that.result.newWinningAmt = '';
                that.result.newBonusAmt = '';
                that.result.newDepositAmt = '';
                that.errorData = angular.copy(defaultError);
                that.result = angular.copy(defaultData);
                that.message = '';
                that.showAlert = false;
                that.userName = '';
            };

            that.onCloseAlert = function () {
                that.showAlert = false;
            };
            return that;

        });
