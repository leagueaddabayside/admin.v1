(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('adminComplianceCtrl', function ($scope, $http, ngDialog, Upload, $rootScope, myConfig, serverApi, UtilityService) {
                var that = this;
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                that.errorMsg = "";
                that.withdrawalList = '';
                var defaultData = {screenName: '', withdrawalAmt: '', remarks: '', userId: ''};
                that.userData = angular.copy(defaultData);
                that.getWithdrawalList = function () {
                    that.showAlert = false;
                    that.message = '';
                    that.messageClass = '';
                    var data = {};
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.WITHDRAWAL_PENDING, data, function (response) {
                        if (response.respCode == 100) {
                            that.withdrawalList = response.respData;
                        } else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                        }
                    });
                }

                that.getWithdrawalList();
                that.setUserDetail = function (data) {
                    that.userData.screenName = data.user.screenName;
                    that.userData.withdrawalAmt = data.fromWinning;
                    that.userData.userId = data.user.userId;
                    that.userData.remark ='';
                    that.userData.status = '';
                    that.userData.txnId = data.txnId;
                     that.errorMsg = "";
                }

                that.onSubmitWithdrawalReq = function () {                   
                    var isValid = true;
                    that.errorMsg = "";
                    that.showAlert = false;
                    that.message = '';
                    that.messageClass = '';
                    var data = {
                        userId: that.userData.userId,
                        fromWinning: that.userData.withdrawalAmt,
                        remarks: that.userData.remark,
                        status: that.userData.status,
                        txnId : that.userData.txnId
                    }
                    if (that.userData.withdrawalAmt <= 0) {
                        isValid = false;
                        that.errorMsg = "Withdrawal amt. cannot be less than 0.";
                        return;
                    }
                    if (that.userData.status == '') {
                        isValid = false;
                        that.errorMsg = "Status cannot be set blank.";
                        return;
                    }
                    if (that.userData.status == 'CRM_C' && that.userData.remark == '') {
                        isValid = false;
                        that.errorMsg = "Remark is require for cancelled request.";
                        return;
                    }
                    if (isValid) {  
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.WITHDRAWAL_UPDATE, data, function (response) {
                            if (response.respCode == 100) {
                                that.showAlert = true;
                                that.message = "Withdrawal status is "+ data.status;
                                that.messageClass = 'alert-success';
                                that.userData = angular.copy(defaultData);
                                that.getWithdrawalList();
                                ngDialog.close();
                            } else {
                                that.showAlert = true;
                                that.message = response.message;
                                that.messageClass = 'alert-danger';
                                that.userData = angular.copy(defaultData);
                                ngDialog.close();
                            }

                        });
                    }
                }
                
                that.getTxnDate = function(time){
                    time = time * 1000;
                    time  = new Date(time);
                    return time.toDateString();
                }
                
                that.onCloseAlert = function () {
                    that.showAlert = false;
                };

                moment.locale('en');

            })

})();