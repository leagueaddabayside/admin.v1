'use strict';

angular.module('adminFantasyApp')
        .controller('gameHistoryReportsCtrl', function ($scope, $log, ReportsService, UtilityService, $rootScope, msgConstants) {
            var that = this;
            var defaultData = {gameId: null, tableId: null, userName: ''};
            that.result = angular.copy(defaultData);
            //that.checked = false;
            function  getCalanderDate() {
                $rootScope.$emit('showloader');
                UtilityService.getServerTime(function (respTime) {
                    $rootScope.$emit('hideloader');
                    var endDate = new Date(respTime.serverTime);
                    var startDate = new Date(respTime.serverTime);
                    startDate.setHours(startDate.getHours() - 1);
                    that.dateRangeStart = startDate;
                    that.dateRangeEnd = endDate;
                });
            }

            getCalanderDate();
            that.getGameHistoryReport = function () {
                that.checked = true;
                that.rowCollection = '';
                that.userNameError = '';
                var userNameDetail = {};
                userNameDetail = {
                    playerName: that.result.userName
                };

                if (that.result.gameId == '') {
                    that.result.gameId = null;
                }
                if (that.result.tableId == '') {
                    that.result.tableId = null;
                }

                var data = {
                    playerId: null,
                    startDate: moment(that.dateRangeStart).format('YYYY-MM-DD HH:mm:ss'),
                    endDate: moment(that.dateRangeEnd).format('YYYY-MM-DD HH:mm:ss'),
                    gameId: that.result.gameId,
                    tableId: that.result.tableId,
                };
                if (that.result.userName != '') {
                    $scope.$emit('showloader');
                    UtilityService.getUserId(userNameDetail, function (respData) {
                        $rootScope.$emit('hideloader');
                        if (respData.respCode == 100) {
                            data.playerId = respData.playerInfo.userId;
                            ReportsService.getGameReport(data, function (respData) {
                                that.rowCollection = respData;
                                that.heading = "Game Log for user " + that.result.userName;
                                that.checked = false;
                            });
                        }
                        else {
                            that.checked = false;
                            that.userNameErrorClass = 'fontcolor-red';
                            that.userNameError = msgConstants.ERR_USER_NAME_REQ;
                        }
                    });

                }
                else {
                    $scope.$emit('showloader');
                    ReportsService.getGameReport(data, function (respData) {
                        $rootScope.$emit('hideloader');
                        that.rowCollection = respData;
                        that.checked = false;
                    });

                }
            };

            that.onResetFormData = function () {
                that.heading = '';
                that.rowCollection = '';
                that.userNameError = '';
                that.dateRangeStart = '';
                that.dateRangeEnd = '';
                getCalanderDate(); 
                that.result = angular.copy(defaultData);
                return;
            };

            //that.controllerName = 'ReportsCtrl';
            moment.locale('en');
        });