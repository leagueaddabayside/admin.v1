'use strict';
angular.module('adminFantasyApp')
        .controller('resetPasswordCtrl', function ($scope, UtilityService, $rootScope, $location, msgConstants) {
            var that = this;
            var defaultData = {userName: '', newPassword: '', confirmNewPassword: ''};
            var defaultErrorData = {userNameError: '', newPasswordError: '', confirmNewPassword: ''};
            that.result = angular.copy(defaultData);
            that.errorData = angular.copy(defaultErrorData);
            that.showAlert = false;

            that.onSubmitFormData = function () {
                that.errorData = angular.copy(defaultErrorData);
                that.message = "";
                that.messageClass = '';
                var isSuccess = true;
                if (that.result.userName == '') {
                    isSuccess = false;
                    that.errorData.userNameError = msgConstants.ERR_USER_NAME_REQ;
                }
                if (that.result.newPassword == '' && that.result.confirmNewPassword == '') {
                    isSuccess = false;
                    that.errorData.newPasswordError = "New Pass. " + msgConstants.ERR_FIELD_BLANK;
                    that.errorData.confirmNewPassword = "Confirm Pass. " + msgConstants.ERR_FIELD_BLANK;
                }

                if (that.result.newPassword != that.result.confirmNewPassword) {
                    isSuccess = false;
                    that.errorData.confirmNewPassword = msgConstants.ERR_NEW_CURR_PSWD_NOT_SAME;
                }
                if (isSuccess) {
                    var userId = '';
                    getuserId(that.result.userName, function (respData) {
                        if (respData.respCode == 100) {
                            $rootScope.$emit('showloader');
                            var data = {};
                            userId = respData.playerInfo.userId;
                            that.result.newPassword = md5(that.result.newPassword);
                            that.result.confirmNewPassword = md5(that.result.confirmNewPassword);

                            data = {
                                userId: userId,
                                newPassword: that.result.newPassword,
                                newConfirmPassword: that.result.confirmNewPassword,
                                token: localStorage.adminToken,
                            };

                            UtilityService.setNewPassword(data, function (respData) {
                                $rootScope.$emit('hideloader');
                                if (respData.resCode == 100) {
                                    that.showAlert = true;
                                    that.message = "Password change successfully.";
                                    that.messageClass = 'alert-success';
                                    that.result = angular.copy(defaultData);
                                    return;
                                }

                                if(respData.resCode == 103){
                                    $location.path('login');
                                    $rootScope.loginSuccess = false;
                                }
                                else {
                                    that.showAlert = true;
                                    that.result = angular.copy(defaultData);
                                    that.message = respData.message;
                                    that.messageClass = 'alert-danger';
                                    return;
                                }
                            });

                        }
                        else{
                            that.errorData.userNameError = respData.message;
                            that.result.newPassword  = '';
                            that.result.confirmNewPassword = '';
                        }
                    });
                }

            };

            that.onResetFormData = function(){
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                that.result = angular.copy(defaultData);
                that.errorData = angular.copy(defaultErrorData);
            };

            that.onCloseAlert = function(){
                that.showAlert = false;
            };

            function getuserId(userName, callback) {
                var data = {};
                data.playerName = userName;
                UtilityService.getUserId(data, function (respData) {
                    callback(respData);
                });
            }
    });
