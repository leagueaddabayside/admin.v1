(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('playerDetailCtrl', function ($http, UtilityService, myConfig, serverApi, msgConstants) {
                var that = this;               
                var defaultData = {isPersonalInfo : false, isAccountinfo : false, isPanInfo : false, isDocumentInfo : false, isWithdrawalInfo:false};
                that.result = angular.copy(defaultData);
                var userDefaultData = {userPersonalInfoData : [], userPanData : [], userAccountsData : [], userDocData : [],}
                that.playerData = angular.copy(userDefaultData);
                that.playerName = '';
                that.showReportMenu = false;
                that.showAlert = false;
                that.getPlayerId = function () {
                    that.userDataErrMsg = '';
                    that.playerName = '';
                    that.isRepostMenu = false;
                    if (that.userName == '') {
                        that.userDataErrMsg = msgConstants.ERR_USER_NAME_REQ;
                        return;
                    }
                    if (that.userName != '') {
                        var data = {
                            userData: that.userName,
                        };
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.USER_ACC_INFO, data, function (response) {
                            if (response.respCode == 100) {
                                that.showReportMenu = true;
                                that.userId = response.respData.userId;
                                 that.getPersonalInfo();
                            } else {
                                that.userDataErrMsg = response.message;
                            }
                        });
                    }

                }

                that.getPersonalInfo = function () {
                    that.showAlert = false;
                    that.result = angular.copy(defaultData);
                    that.playerData = angular.copy(userDefaultData);
                    var data = {};
                    data.userId = that.userId;
                    data.requestType = "personalInfo";
                    if (that.userId == '') {
                        return;
                    }
                    UtilityService._postAjaxCall(myConfig.reportServer + serverApi.FETCH_PLAYER_INFO, data, function (response) {
                        if (response.respCode == 100) {
                            that.playerData.userPersonalInfoData = response.respData;
                            that.playerName = response.respData.firstName + ' ' + response.respData.lastName;
                            that.result.isPersonalInfo = true;
                            return;
                        }
                        if (response.respCode == 110) {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }else{
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                        }
                    });
                };

                that.getPanInfo = function () {
                    that.showAlert = false;
                    that.result = angular.copy(defaultData);
                    that.playerData = angular.copy(userDefaultData);
                    var data = {};
                    data.userId = that.userId;
                    data.requestType = "panInfo";
                    UtilityService._postAjaxCall(myConfig.reportServer + serverApi.FETCH_PLAYER_INFO, data, function (response) {
                        if (response.respCode == 100) {
                            that.result.isPanInfo = true;
                            that.playerData.userPanData = response.respData;
                            return;
                        }
                        if (response.respCode == 110) {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                    });
                };


                that.getAccountsInfo = function () {
                    that.showAlert = false;
                    that.result = angular.copy(defaultData);
                    that.playerData = angular.copy(userDefaultData);
                    var data = {};
                    data.userId = that.userId;
                    data.requestType = "accountInfo";
                    UtilityService._postAjaxCall(myConfig.reportServer + serverApi.FETCH_PLAYER_INFO, data, function (response) {
                        if (response.respCode == 100) {
                            that.result.isAccountInfo = true;
                            that.playerData.userAccountsData = response.respData;
                            return;
                        } 
                        if (response.respCode == 110) {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                        else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                    });
                };

                that.getDocumentInfo = function () {
                    that.showAlert = false;
                    that.result = angular.copy(defaultData);
                    that.playerData = angular.copy(userDefaultData);
                    var data = {};
                    data.userId = that.userId;
                    data.requestType = "docInfo";
                    UtilityService._postAjaxCall(myConfig.reportServer + serverApi.FETCH_PLAYER_INFO, data, function (response) {
                        if (response.respCode == 100) {
                            that.result.isDocumentInfo = true;
                            that.playerData.userDocData = response.respData;
                            return;
                        } 
                        if (response.respCode == 110) {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                        else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }
                    });
                };
                              
                
                that.getDateFormat = function (time) {
                    time = time * 1000;
                    time = new Date(time);
                    return time.toDateString();
                }
                
                that.onCloseAlert = function () {
                    that.showAlert = false;
                };
            
            
            });
})();

