(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('pancardVerifyCtrl', function ($scope, $http, ngDialog, Upload, $rootScope, UtilityService, myConfig, msgConstants, serverApi) {
                var that = this;
                that.showAlert = false;
                that.showVerifyBtn = true;
                that.status = '';
                var userPan = '';
                that.pancardList = [];
                that.imageUrl = myConfig.websiteUrl;
                var panData = {};
                that.getPendingPanList = function (item) {
                    that.message = '';
                    that.messageClass = '';
                    that.showAlert = false;
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.PAN_PENDING, panData, function (response) {
                        if (response.respCode == 100) {
                            that.pancardList = response.respData;
                        } else {
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            that.showAlert = true;
                        }
                    });
                }

                that.getPendingPanList();
                
                function checkServerResp(response){
                    that.userPanDetail.userId = '';
                    that.userPanDetail.panNumber = '';
                    that.status = '';
                    that.remark = '';
                    if (response.respCode == 100) {
                            ngDialog.close();
                            that.message = msgConstants.SUCCESS_SERVER_RESP_UPDATE;
                            that.messageClass = 'alert-success';
                            that.showAlert = true;
                            that.status = '';
                            that.userPanDetail.status = '';
                            that.getPendingPanList();
                        } else {
                            ngDialog.close();
                            that.status = '';
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            that.showAlert = true;
                            that.getPendingPanList();
                        }
                }

                that.onSubmitPanStatusData = function () {
                    that.message = '';
                    that.messageClass = '';
                    that.showAlert = false;
                    that.errorMsg = '';
                    var isValid = true;
                    var data = {
                        userId: that.userPanDetail.userId,
                        panNumber: that.userPanDetail.panNumber,
                        status: that.userPanDetail.status,
                        remarks: that.remark,
                        token: localStorage.adminToken,
                    }
                    if (that.userPanDetail.status == 'PENDING') {
                        that.errorMsg = msgConstants.ERR_STATUS_CANNOT_PENDING;
                        isValid = false;
                        return;
                    }
                    if (that.userPanDetail.status == '') {
                        that.errorMsg = msgConstants.ERR_POPUP_STATUS_BLANK;
                        isValid = false;
                        return;
                    }
                    if (that.userPanDetail.status == 'FAILED' && that.remark == '') {
                        that.errorMsg = msgConstants.ERR_REMARKS_REQ;
                        isValid = false;
                        return;
                    }
                    if (isValid) {
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.PAN_VERIFY, data, function (response) {
                                checkServerResp(response)
                            });     
                    }
                }
                
                that.getDob = function (time) {
                    time = new Date(time);
                    return time.toDateString();
                }

                that.onCloseAlert = function () {
                    that.showAlert = false;
                };

                that.openDialog = function (panDetail) {
                    userPan = '';
                    that.userPanDetail = '';
                    that.remark = '';
                    that.showUpdateBtn = false;
                    that.showVerifyBtn = true;
                    that.userPanDetail = panDetail;
                    userPan = panDetail.panNumber;
                }

                moment.locale('en');

            })

})();