'use strict';
angular.module('adminFantasyApp')
        .controller('UserTxnReportsCtrl', function ($scope, UtilityService, $rootScope, $timeout, pagination, msgConstants, myConfig, serverApi) {
            var that = this;
            getCalanderDate();
            var defaultMsg = {reportErrorMsg: '', userNameError: ''};
            var defaultData = {userId: '', pageIndex: 1, pageCount: [], pageSize: 10, transactionList: [], txnList: [], itemList: [],
                showForm: true, showAlert: false, isUserNameError: false, isReportError: false, };
            that.result = angular.copy(defaultData);
            that.errorMsg = angular.copy(defaultMsg);

            function  getCalanderDate() {
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.SERVER_TIME, data, function (response) {
                    if (response.respCode == 100) {
                        var endDate = new Date(response.serverTime);
                        var startDate = new Date(response.serverTime);
                        startDate.setDate(startDate.getDate() - 1);
                        that.dateRangeStart = startDate;
                        that.dateRangeEnd = endDate;
                    }

                });
            }

            that.getUserDetail = function () {
                that.result.showAlert = false;
                var totalBalance = 0;
                if (that.userName == '') {
                    that.errorMsg.userNameError = msgConstants.ERR_USER_NAME_REQ;
                    return;
                }
                var data = {
                    userData: that.result.userName,
                    token: localStorage.adminToken,
                };

                UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.USER_ACC_INFO, data, function (response) {
                    if (response.respCode == 100) {
                        that.result.isUserNameError = false;
                        that.result.userId = response.respData.userId;
                    } else {
                        that.result.isUserNameError = true;
                        that.errorMsg.userNameError = msgConstants.ERR_USER_NAME_INVALID;
                    }
                });
            }

            that.getTxnReport = function () {
                that.result.isReportError = false;
                that.result.reportErrorMsg = "";
                that.result.transactionList = [];
                var fromDate = new Date(that.dateRangeStart).getTime() / 1000;
                var toDate = parseInt(new Date(that.dateRangeStart).getTime() / 1000);
                var data = {
                    userId: that.result.userId,
                    fromDate: parseInt(new Date(that.dateRangeStart).getTime() / 1000),
                    toDate: parseInt(new Date(that.dateRangeEnd).getTime() / 1000),
                }
                if (that.result.userId == '') {
                    that.result.isUserNameError = true;
                    that.errorMsg.userNameError = msgConstants.ERR_USER_NAME_INVALID;
                    return;
                }
                UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.TXN_REPORT_LIST, data, function (response) {
                    if (response.respCode == 100) {
                        that.result.transactionList = response.respData;
                        that.result.showForm = false;
                        that.setPageSize();
                    } else {
                        that.result.isReportError = true;
                        that.errorMsg.reportErrorMsg = msgConstants.NO_RECORD_FOUND;
                        that.result.transactionList = [];
                        that.result.showForm = true;
                    }

                })
            };

            that.onResetFormData = function () {
                that.result = angular.copy(defaultData);
                that.errorMsg = angular.copy(defaultMsg);
                getCalanderDate();
            };

            that.setPageSize = function () {
                that.result.pageCount = [];
                var data = {
                    pageCount: that.result.pageCount,
                    listLength: that.result.transactionList.length,
                    pageSize: that.result.pageSize,
                }
                pagination.setPageSize(data, function (response) {
                    $timeout(function () {
                        that.getPageList(1);
                    }, 10);
                    that.result.pageCount = response.pageCount;
                    that.result.pageSize = response.pageSize;

                });
            }

            that.getPageList = function (pageNo) {
                that.activeClass = '';
                var data = {
                    pageNo: pageNo,
                    newPageListData: [],
                    pageCount: that.result.pageCount,
                    recordData: that.result.transactionList,
                    pageSize: that.result.pageSize,
                }
                pagination.getPageList(data, function (response) {
                    that.showForm = false;
                    that.activeClass = response.activeClass;
                    that.result.txnList = response.newPageListData;
                    that.result.pageIndex = pageNo - 1;
                });
            }

            that.showHideFormFields = function (data) {
                if (data == 'hide-form') {
                    that.result.showForm = false;
                }
                if (data == 'show-form') {
                    that.result.showForm = true;
                }
            }

            that.getTxnDate = function (time) {
                time = time * 1000;
                time = new Date(time);
                return time.toDateString();
            }

            moment.locale('en');
        });

