'use strict';
angular.module('adminFantasyApp')
        .controller('LeagueReportsCtrl', function ($scope, UtilityService, $rootScope, myConfig, serverApi) {
            var that = this;
            that.tourList = [];
            that.showAlert = false;
            that.message = '';
            that.messageClass = '';
            var defaultData = {matchId : '', templateList : [], matchList : [], reportList : [],configList : [], configId : '', 
                isDefaultLeagueReport: true };       
            that.result =angular.copy(defaultData);
            var getTourAndMatchData = function () {
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response)  {
                    if (response.respCode == 100) {
                        for (var i = 0; i < response.respData.length; i++) {
                            if (response.respData[i].status != 'PENDING') {
                                that.tourList.push(response.respData[i]);
                            }
                        }
                    }
                    else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                    
                });               
            };
            getTourAndMatchData(); 
            
            that.getMatchList = function () {
                that.result =angular.copy(defaultData);
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                var data = {
                    tourId: that.tourId,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_LIST, data, function (response){
                    if (response.respCode == 100) {
                        that.result.matchList = response.respData;                          
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            };
            
            that.getConfiglist = function(){
               that.result.reportList = [];
               that.result.configList = [];
               that.showAlert = false;
               that.message = '';
               that.messageClass = '';
               var data = {
                    matchId: that.result.matchId,
               } 
               UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.LEAGUE_CONFIG_LIST, data, function (response){
                   if(response.respCode ==100){
                        that.result.configList = response.respData;
                   }
                   else{
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                   
                }); 
            }
            
            that.getReports = function(){
               var data = {
                    matchId:  that.result.matchId
                }

                if(that.result.leagueType && that.result.leagueType !== ''){
                    data.leagueType = that.result.leagueType;
                }

                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
                if(that.result.configId == ''){
                    UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.LEAGUE_CONFIG_REPORT, data, function (response)  { 
                        if (response.respCode == 100) {
                            that.result.isDefaultLeagueReport = true;
                            that.result.reportList = response.respData;
                        } else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                        }
                        
                    });
                    
                }
                
                if(that.result.configId != ''){
                  data.configId = that.result.configId;
                    UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.LEAGUE_REPORT_LIST, data, function (response)  {
                        if (response.respCode == 100) {
                            that.result.isDefaultLeagueReport = false;
                            that.result.reportList = response.respData;
                        } else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                        }
                    });
                }
                                  
            }
            
            
            
            that.onCloseAlert = function () {
                that.showAlert = false;
            };

           // that.controllerName = 'TransactionReportsCtrl';
            moment.locale('en');
});

