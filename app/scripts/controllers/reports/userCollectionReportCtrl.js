'use strict';
angular.module('adminFantasyApp')
        .controller('userCollectionReportCtrl', function ($scope, UtilityService, myConfig, serverApi, msgConstants) {
            var that = this;
            
            var defaultMsg = {reportErrorMsg: ''};
            that.dataType = '';
            that.reportType = '';
            that.dateRangeEnd = new Date();
            that.dateRangeStart = new Date(that.dateRangeEnd);
            that.dateRangeStart.setDate(that.dateRangeEnd.getDate() -1);
            
            
            
            that.getCollectionReport = function () {              
                var fromDate = '';
                var toDate = '';
                fromDate = new Date(that.dateRangeStart).getTime() / 1000;                
                toDate = parseInt(new Date(that.dateRangeEnd).getTime() / 1000); 
                var data = {};
                if(that.reportType == ''){
                    that.errorMsg ='\n\n' +  "Report type is require.";
                    return;
                }
                else if(that.dataType == ''){
                    that.errorMsg = '\n' + "Data Type is require.";
                    return;
                }               
                else{
                   data = {
                   fromDate : fromDate,
                   toDate : toDate,
                   }  
                }              
                if(that.reportType == 'dateWise'){
                   window.open(myConfig.reportServer + '/getDateCollection?startDate=' +data.fromDate+'&endDate='+data.toDate+'&dataType='+that.dataType); 
                }
                if(that.reportType == 'userWise'){
                    window.open(myConfig.reportServer + '/getUserCollection?startDate=' +data.fromDate+'&endDate='+data.toDate+'&dataType='+that.dataType);
                }
            };

            that.getTxnDate = function (time) {
                time = time * 1000;
                time = new Date(time);
                return time.toDateString();
            }
            moment.locale('en');
        });

