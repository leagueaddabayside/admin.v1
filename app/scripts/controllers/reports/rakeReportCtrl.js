'use strict';

angular.module('adminFantasyApp')
        .controller('rakeReportCtrl', function ($scope, UtilityService, msgConstants, myConfig, serverApi) {
            var that = this;
            that.tourList = [];
            that.rakeReportList = [];
            that.tourId = '';
            that.matchId = '';
            that.rakeType = '';
            that.isRakeDataExist = false;
            var getTourAndMatchData = function () {
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response)  {
                    if (response.respCode == 100) {
                        for (var i = 0; i < response.respData.length; i++) {
                            if (response.respData[i].status !="PENDING") {
                                that.tourList.push(response.respData[i]);
                            }
                        }
                    }
                    else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                    
                });               
            };
            getTourAndMatchData(); 
            
            that.getMatchList = function () {
                var data = {
                    tourId: that.tourId,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_LIST, data, function (response){
                    if (response.respCode == 100) {
                        that.matchList = response.respData;                          
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            };
            
            
            that.getRakeReport = function () {
                that.errorMsg= '';
                that.isRakeDataExist = false;
                var data = {};
                if(that.tourId == ''){
                 that.errorMsg = msgConstants.ERR_TOUR_NOT_SELECT;
                 return;
                }
                else if(that.matchId == ''){
                  that.errorMsg = msgConstants.ERR_MATCH_NOT_SELECT;
                  return;
                }
                else if(that.rakeType == ''){
                   that.errorMsg = msgConstants.ERR_RAKE_TYPE_NOT_SELECT;
                   return;
                }
                else{
                    data = {
                    tourId: that.tourId,
                    matchId : that.matchId,
                    rakeType : that.rakeType
                    }
                }
                UtilityService._postAjaxCall(myConfig.reportServer + serverApi.FETCH_RAKE_REPORT, data, function (response) {
                    if (response.respCode == 100) {
                        that.rakeReportList = response.respData;
                        if(that.rakeReportList.length ==0){
                            that.isRakeDataExist = true;
                        }
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            }
            
            that.getDateFormat = function (time) {
                time = time * 1000;
                time = new Date(time);
                return time.toDateString();
            }
            
            that.onCloseAlert = function () {
                that.showAlert = false;
            };

           
});
