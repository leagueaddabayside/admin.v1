'use strict';

angular.module('adminFantasyApp')
        .controller('revenueReportsCtrl', function ($scope, UtilityService, $rootScope, msgConstants, myConfig, serverApi) {
            var that = this;
            var defaultData = {userName: '', configId: '-1'};
            that.result = angular.copy(defaultData);

            function  getCalanderDate() {
                var data = {};
               UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.SERVER_TIME, data, function (response) {
                    var endDate = new Date(response.serverTime);
                    var startDate = new Date(response.serverTime);
                    startDate.setDate(startDate.getDate() - 7);
                    that.dateRangeStart = startDate;
                    that.dateRangeEnd = endDate;
                });
            }

            getCalanderDate();
            UtilityService.getGameConfigId(function (respData) {
                if (respData.resCode == 100) {
                    that.configCollection = respData.data;
                }
            });

            that.getRevenueReport = function () {
                that.rowCollection = '';
                that.userNameError = '';
                var userNameDetail = {};
                userNameDetail = {
                    playerName: that.result.userName,
                };
                var data = {
                    playerId: null,
                    startDate: moment(that.dateRangeStart).format('YYYY-MM-DD'),
                    endDate: moment(that.dateRangeEnd).format('YYYY-MM-DD'),
                };

                if (that.result.configId !== "-1") {
                    data.configId = that.result.configId;
                }

                if (that.result.userName != '') {
                    $rootScope.$emit('showloader');
                    UtilityService.getUserId(userNameDetail, function (respData) {
                        $rootScope.$emit('hideloader');
                        if (respData.respCode == 100) {                          
                            data.playerId = respData.playerInfo.userId;
                            ReportsService.getRevenueReport(data, function (respData) {
                                that.result = angular.copy(defaultData);                               
                                that.rowCollection = respData;
                            });
                        }
                        else {
                            that.rowCollection = '';
                            that.userNameErrorClass = 'fontcolor-red';
                            that.userNameError = msgConstants.ERR_USER_NAME_INVALID;
                        }
                    });
                }
                else {
                    $rootScope.$emit('showloader');
                    ReportsService.getRevenueReport(data, function (respData) {
                        $rootScope.$emit('hideloader');
                        that.rowCollection = respData;
                    });
                }
            };

            that.onResetFormData = function(){
                that.dateRangeStart = '';
                that.dateRangeEnd = '';
                that.rowCollection = '';
                that.userNameError = '';
                that.result = angular.copy(defaultData);
                that.configId = null;
                getCalanderDate();
            };

            //that.controllerName = 'ReportsCtrl';
            moment.locale('en');
});
