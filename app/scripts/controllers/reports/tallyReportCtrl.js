(function () {
    'use strict';
     angular.module('adminFantasyApp')
            .controller('tallyReportCtrl', function (UtilityService, myConfig, serverApi) {
             var that = this;
             var defaultData = {matchId : '',matchList : [], txnStatus : 'FAILED', txnType : '', reportList : [], gameTxnReportList : [], rakeAmountList:[], accountTallyList :[]};       
             that.result =angular.copy(defaultData);  
             that.isRakeReport = false;
             that.isGameTxn = false;
             that.isAccountTally = false;
             
             var getTourAndMatchData = function () {
                that.result =angular.copy(defaultData); 
                that.tourList = [];
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response)  {
                    if (response.respCode == 100) {
                        for (var i = 0; i < response.respData.length; i++) {
                            if (response.respData[i].status != 'PENDING') {
                                that.tourList.push(response.respData[i]);
                            }
                        }
                    }
                    else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });               
            };
            
            
            that.getMatchList = function () {
                that.result =angular.copy(defaultData);
                 var data = {
                    tourId: that.tourId,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_LIST, data, function (response){
                    console.log("Match List",response);
                    if (response.respCode == 100) {
                        that.result.matchList = response.respData;                          
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            };
            
            that.getAccountTally = function(){
                that.result =angular.copy(defaultData); 
                that.isRakeReport = false;
                that.isGameTxn = false;
                that.isAccountTally = true;
                var data = {};
                UtilityService._postAjaxCall(myConfig.reportServer + serverApi.ACCOUNT_TALLY, data, function (response){
                    if (response.respCode == 100) {
                        that.result.accountTallyList = response.respData;                          
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            }
            
            that.showRakeTallyForm = function(){
                that.result =angular.copy(defaultData); 
                that.isRakeReport = true;
                that.isGameTxn = false;
                that.isAccountTally = false;
                getTourAndMatchData(); 
            }
            
            that.getRakeTallyList = function(){
                var data = {};
                if( that.result.tourId == ''){
                    
                    return;
                }
                if( that.result.matchId == ''){
                    return;
                }
                
                data.matchId = that.result.matchId;
                
                 UtilityService._postAjaxCall(myConfig.reportServer + serverApi.RAKE_TALLY, data, function (response){
                     console.log("rake response:::",response);
                    if (response.respCode == 100) {
                        that.rakeTallyRow = response.respData;
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }

                });
            }
            
            that.showGameTxnReportForm = function(){
                that.isGameTxn = true;
                that.isRakeReport = false;
                that.isAccountTally = false;
                that.result =angular.copy(defaultData); 
            }
            
            that.getGameTxnReport = function(){
                var data = {};
                data.status = that.txnStatus ;
                data.txnType = that.txnType;
                
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.GAME_TXN_REPORT, data, function (response){
                    if (response.respCode == 100) {
                        that.result.gameTxnReportList = response.respData;                     
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            } 
            });   
})();

