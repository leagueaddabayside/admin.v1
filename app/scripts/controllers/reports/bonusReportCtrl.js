'use strict';

angular.module('adminFantasyApp')
        .controller('bonusReportCtrl', function ($scope, UtilityService, $rootScope, msgConstants, myConfig, serverApi,$state) {
            var that = this;
            var defaultData = {userName: '', configId: '-1'};
            that.result = angular.copy(defaultData);

            that.getBonusReport = function () {
               that.errorMsg = '';
                var fromDate = new Date(that.dateRangeStart).getTime() / 1000;
                var toDate = parseInt(new Date(that.dateRangeStart).getTime() / 1000);
                var data = {
                    fromDate: parseInt(new Date(that.dateRangeStart).getTime() / 1000),
                    toDate: parseInt(new Date(that.dateRangeEnd).getTime() / 1000),
                }
                
                if(data.fromDate && data.toDate){
                     window.open(myConfig.reportServer + '/reports/bonus?startDate=' +data.fromDate+'&endDate='+data.toDate);
                }
                else{
                    that.result.isDateError = true;
                    that.errorMsg = "Date field cannot be empty.";
                    return;  
                }
               
            };
         

            that.getDateFormat = function (time) {
                time = time * 1000;
                time = new Date(time);
                return time.toDateString();
            }

            moment.locale('en');
           
});
