'use strict';
angular.module('adminFantasyApp')
        .controller('leagueTemplateCtrl', function ($scope, toursconstants, UtilityService, msgConstants, myConfig, serverApi) {
            var that = this;
            that.showEntryWinAmt = false;
            that.isFormValid = true;
            that.showAlert = false;
            
            var defaultData = {templateName: null, chipType: null, totalPrizePoolAmt: '', leagueData: '',
                maxTeamRegister: '', commissionRate: '', maxLeagueCreate: '', isMultiTeamAllowed: false,
                thresholdTeam: '', entryAmt: '', winningAmt: '', isNotConfirmedLeague: false, prizeTableList :[], rankField: '', isDefaultLeague : false,
            }
            
            var defaultErrorMsg = {
                leagueNameErrorMsg: '', totalPrizePoolAmtErrorMsg: '', chipTypeErrorMsg: '',
                leagueSizeErrorMsg: '', commiRateErrorMsg: '',
                maxLeagueCreateErrorMsg: '', thresholdTeamErrorMsg: '',prizeListCountErrorMsg :'',invalidRankErrorMsg : '',
            };
            that.errorMsg = angular.copy(defaultErrorMsg);
            that.result = angular.copy(defaultData);
            that.result.prizeTableList = [];
            that.prizeList = [];
            that.prizeListCount = [];
            that.leagueSizeList = toursconstants.LEAGUE_SIZE_AND_COMMISSION;
            
            //Get Template Name List that already created.
            var getTemplateLsit = function(){
                that.showAlert = false;
                that.message = '';
                that.messageClass = '';
               var data ={
                    token : localStorage.adminToken,
                };
               UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.LEAGUE_TEMPLATE_LIST, data, function (response)  { 
                if (response.respCode == 100) {
                    that.leagueTemplateList = response.respData;
                } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                }
            }); 
            }
            
            getTemplateLsit();
            var createRankFields = function(length){
                that.result.prizeTableList = [];
                for(var i=0; i<length; i++){
                        that.result.prizeTableList.push(1);
                        that.prizeListCount += 1;
                    }
            }

            //Get Template Data for selected template.
            that.fetchTemplateData = function (row) {
                 that.errorMsg = angular.copy(defaultErrorMsg);
                 that.result = angular.copy(defaultData);
                 that.showAlert = false;
                 that.message = '';
                 that.messageClass = '';
                 that.prizeList = [];
                 that.prizeListCount = '';
                if (row == '') {                   
                    return;
                }
                row = JSON.parse(row);
                var data = {
                    templateId: row.templateId,
                    token : localStorage.adminToken,
                }
                that.templateSelected = row.templateName;
                that.result.templateName = row.templateName;
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.LEAGUE_TEMP_DATA_BY_ID, data, function (response) {
                    if (response.respCode == 100) {
                        that.result = response.respData.data;
                        that.result.templateName = response.respData.templateName;
                        that.result.maxTeamRegister = String(response.respData.data.maxTeamRegister);
                        that.result.thresholdTeam = Number(response.respData.data.thresholdTeam);
                        if (response.respData.data.chipType == 'real') {
                            that.result.totalPrizePoolAmt = response.respData.data.prizeDetails.totalPrizePoolAmt;
                            that.prizeList = response.respData.data.prizeDetails.prizePoolList;
                            that.prizeListCount = response.respData.data.prizeDetails.prizePoolList.length;
                            that.result.prizeTableList = response.respData.data.prizeDetails.prizePoolList.length;
                            createRankFields(that.prizeListCount);
                            that.calcEntryAndWinAmt();
                        }
                    }
                    else{
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            }
            
            that.restPrizePool = function () {
                that.result.totalPrizePoolAmt ='';
                that.prizeList = [];
                that.prizeListCount = '';
                that.result.prizeTableList = '';
                if( that.result.chipType == 'skill'){
                    that.showEntryWinAmt = false;
                }
            }

            that.calcEntryAndWinAmt = function () {
                that.commiRateErrorMsg = '';
                var isValid = true;
                if (that.result.chipType == 'real' && that.result.commissionRate < 0 || that.result.commissionRate > 100) {
                    that.errorMsg.commiRateErrorMsg = msgConstants.ERR_COMMI_RATE_INVALID;
                    isValid = false;
                    return false;
                }
                if (that.result.maxTeamRegister >= 2 && isValid) {
                    var intrestAmt = ((that.result.totalPrizePoolAmt * that.result.commissionRate) / 100).toFixed(2);
                    that.result.winningAmt = that.result.totalPrizePoolAmt - intrestAmt;
                    that.result.entryAmt = (that.result.totalPrizePoolAmt / that.result.maxTeamRegister).toFixed(2);
                    that.showEntryWinAmt = true;                   
                    that.errorMsg.prizeListCountErrorMsg = '';
                    return;
                } else {
                    that.showEntryWinAmt = false;
                }                            
            }

            // Get Commission Rate from HArd Code JSON Data.
            that.getCommissionRate = function (maxTeam) {
                that.result.thresholdTeam = maxTeam;
                for(var i = 0; i < that.leagueSizeList.length ; i++){
                  if(that.leagueSizeList[i].maxTeamRegister == maxTeam)  {
                      that.result.commissionRate = that.leagueSizeList[i].commissionRate;
                  }
                }
                that.calcEntryAndWinAmt();
            }

            // Set the winner prize count.
            that.addRankField = function () {
                var isRankFieldValid = true;
                that.errorMsg.prizeListCountErrorMsg = ''; 
                if(that.result.chipType != 'real'){
                    that.errorMsg.prizeListCountErrorMsg = msgConstants.ERR_PRIZE_POOL_REAL;
                    isRankFieldValid = false;
                    return isRankFieldValid;
                }
                if(that.result.maxTeamRegister == ''){
                    that.errorMsg.prizeListCountErrorMsg = msgConstants.ERR_MAX_TEAM_ALLOWED;
                    isRankFieldValid = false;
                    return isRankFieldValid;
                }
                if(that.showEntryWinAmt == false){
                    that.errorMsg.prizeListCountErrorMsg = msgConstants.ERR_WINNING_NO_SET;
                    return false;
                }
                if(that.result.prizeTableList.length == that.result.maxTeamRegister){
                    that.errorMsg.prizeListCountErrorMsg = msgConstants.ERR_PRIZE_DESTRI; 
                    isRankFieldValid = false;
                    return isRankFieldValid;
                }
                if(that.result.rankField <=0){
                    that.errorMsg.prizeListCountErrorMsg = msgConstants.ERR_RANK_FIELD_INVALID; 
                    isRankFieldValid = false;
                    return isRankFieldValid;
                }
                if(isRankFieldValid){ 
                    createRankFields(that.result.rankField);                   
                    
                }
            }

            // Add array to prize list.
            that.addPrizeRow = function () {
                that.result.prizeTableList.push(1);
                that.prizeListCount += 1;
            }


            // Delete Row from prize list section.
            that.deletePrizeRow = function (index) {
                that.result.prizeTableList.splice(index, 1);
                that.prizeList.splice(index, 1);
                that.prizeListCount -= 1;
            }
            
            that.setMinTeamThreshold = function(){          
                if(that.result.isNotConfirmedLeague==true){
                    that.result.thresholdTeam = that.result.maxTeamRegister;
                    return;
                }
            }

            //Validate Prize Pool Data
            var valiadatePrizePool = function (data) {
                var prevEndRank = 1;
                var isRankValid = true;             
                var totalPrize = 0;
                var firstRankerWinAmt = 0;
                that.prizeListCount = data.length;
                that.lastRank = '';
                for (var i = 0; i < data.length && that.result.chipType == 'real'; i++) {                  
                    if (i == 0) {
                        if (data[i].startRank != 1 || data == undefined || data[data.length - 1].startRank > that.result.maxTeamRegister || data[data.length - 1].toRank > that.result.maxTeamRegister) {
                            isRankValid = false;
                            that.errorMsg.invalidRankErrorMsg = msgConstants.ERR_RANK_DESTRI_INVALID;
                            return isRankValid;
                        }
                    } else {
                        if ((data[i].startRank - 1) != prevEndRank) {
                            isRankValid = false;
                            that.errorMsg.invalidRankErrorMsg = msgConstants.ERR_RANK_DESTRI_INVALID;
                            return isRankValid;
                        }
                        if (data[i].toRank > 1 && data[i].startRank >= data[i].toRank) {
                            isRankValid = false;
                            that.errorMsg.invalidRankErrorMsg = msgConstants.ERR_SRANK_TORANK;
                            return isRankValid;
                        }

                    }
                    //Set Previous Rank.                   
                    if (data[i].toRank > 1) {
                        prevEndRank = data[i].toRank;
                        that.lastRank = data[i].toRank;
                        totalPrize = totalPrize + data[i].prizeAmt * (data[i].toRank - data[i].startRank + 1); 
                        if (totalPrize <= 0 || totalPrize > that.result.winningAmt) {
                            isRankValid = false;
                            that.errorMsg.invalidRankErrorMsg = msgConstants.ERR_PRIZE_POOL_ERR;
                            return isRankValid;
                        }
                    } else {
                       that.lastRank = data[i].startRank;
                        prevEndRank = data[i].startRank;
                        totalPrize = totalPrize + data[i].prizeAmt;
                        if (totalPrize <= 0 || totalPrize > that.result.winningAmt) {
                            isRankValid = false;
                            that.errorMsg.invalidRankErrorMsg = msgConstants.ERR_PRIZE_POOL_ERR;
                            return isRankValid;
                        }

                    }

                }
                if (totalPrize != that.result.winningAmt && that.result.chipType == 'real') {
                    isRankValid = false;
                    that.errorMsg.invalidRankErrorMsg = msgConstants.ERR_RANK_AMT;
                    return isRankValid;
                }
                return isRankValid;
            };

            var formValidation = function () {
                that.isFormValid = true;
                if (that.result.templateName == '' || that.result.templateName == null) {
                    that.errorMsg.leagueNameErrorMsg = msgConstants.ERR_TEMPLATE;
                    that.isFormValid = false;
                    return that.isFormValid; 
                }
                if (that.result.chipType == '' || that.result.chipType == null) {
                    that.errorMsg.chipTypeErrorMsg = msgConstants.ERR_LEAGUE_CHIP_TYPE;
                    that.isFormValid = false;
                    return that.isFormValid;
                }
                if(that.result.chipType == 'skill'){
                     that.result.entryAmt = 0;
                     that.result.commissionRate = 0;
                     that.prizeListCount = 0;
                     that.result.totalPrizePoolAmt = 0;
                     that.prizeList =[];     
                }
                if (that.result.chipType == 'real') {
                    if (that.result.totalPrizePoolAmt == '' || that.result.totalPrizePoolAmt == null) {
                        that.errorMsg.totalPrizePoolAmtErrorMsg = msgConstants.ERR_PRIZE_POOL_EMPTY;
                        that.isFormValid = false;
                        return that.isFormValid;
                    }
                    if (that.result.maxTeamRegister == '' || that.result.maxTeamRegister == undefined) {
                        that.errorMsg.leagueSizeErrorMsg = msgConstants.ERR_LEAGUE_SIZE;
                        that.isFormValid = false;
                        return that.isFormValid;
                    }
                    if (that.result.commissionRate == '' || that.result.commissionRate < 0) {
                        that.errorMsg.commiRateErrorMsg = msgConstants.ERR_COMM_RATE;
                        that.isFormValid = false;
                        return  that.isFormValid;
                    }
                }
                if (that.result.maxLeagueCreate < 1) {
                    that.errorMsg.maxLeagueCreateErrorMsg = msgConstants.ERR_MAX_LEAGUE;
                    that.isFormValid = false;
                    return that.isFormValid;
                }
                if (that.result.isNotConfirmedLeague == true && (Number(that.result.thresholdTeam) < 2 || Number(that.result.thresholdTeam) > that.result.maxTeamRegister)) {
                    that.errorMsg.thresholdTeamErrorMsg = msgConstants.ERR_MIN_TEAM;
                    that.isFormValid = false;
                    return that.isFormValid;
                }
                return that.isFormValid;
            }

            that.onSubmitTemplateForm = function () {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                that.errorMsg = angular.copy(defaultErrorMsg);
                var isPrizeListValid = valiadatePrizePool(that.prizeList);                
                var reqData = {};               
                formValidation();
                if (that.isFormValid && isPrizeListValid) {
                    reqData = {
                        templateName: that.result.templateName,
                        data: {
                            entryAmt: that.result.entryAmt,
                            chipType: that.result.chipType,                            
                            maxTeamRegister: that.result.maxTeamRegister,
                            maxLeagueCreate: that.result.maxLeagueCreate,
                            isMultiTeamAllowed: that.result.isMultiTeamAllowed,
                            isNotConfirmedLeague: that.result.isNotConfirmedLeague,
                            thresholdTeam: Number(that.result.thresholdTeam),
                            isDefaultLeague: that.result.isDefaultLeague,  
                            commissionRate: that.result.commissionRate,
                            token : localStorage.adminToken,
                            prizeDetails: {
                                    prizeListCount: that.lastRank,
                                    totalPrizePoolAmt: that.result.totalPrizePoolAmt,
                                    prizePoolList: that.prizeList,
                                    winningAmt : that.result.winningAmt,
                                }
                        }
                    };
                    if (that.templateSelected != that.result.templateName) {
                        console.log("REQ Data",reqData);
                         UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TEMPLATE_DATA_SAVE, reqData, function (response) { 
                            console.log("Template Data",response);
                            if (response.respCode == 100) { 
                                that.errorMsg = angular.copy(defaultErrorMsg);
                                that.result = angular.copy(defaultData);
                                that.prizeListCount= '';
                                that.result.prizeTableList = '';
                                that.prizeList = [];
                                that.result.leagueData =  '';
                                that.showAlert = true;                               
                                that.messageClass = 'alert-success';
                                that.message = msgConstants.TEMPLATE_SAVE_SUCCESS;
                                that.templateData = '';
                            } else {
                                that.showAlert = true;
                                that.message = response.message;
                                that.messageClass = 'alert-danger';
                            }
                        });
                    } else {
                        var data = JSON.parse(that.templateData);
                        reqData.templateId = data.templateId;
                        UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TEMPLATE_DATA_UPDATE, reqData, function (response)  {
                            console.log("Template Data",response);
                            if (response.respCode == 100) {
                                that.errorMsg = angular.copy(defaultErrorMsg);
                                that.result = angular.copy(defaultData);
                                that.prizeListCount = '';
                                that.result.prizeTableList = '';
                                that.prizeList = '';
                                that.result.leagueData = '';
                                that.showAlert = true;                               
                                that.messageClass = 'alert-success';
                                that.message = msgConstants.TEMPLATE_UPDATE_SUCCESS;
                                return;
                            } else {
                                that.showAlert = true;
                                that.message = response.message;
                                that.messageClass = 'alert-danger';
                            }
                        });
                    }

                }
            }
            that.onCloseAlert = function(){
                that.showAlert = false;
            };
        });
