'use strict';
angular.module('adminFantasyApp')
        .controller('createLeagueCtrl', function ($scope, UtilityService, msgConstants, myConfig, serverApi) {
            var that = this;
            that.tourList = [];
            var defaultData = {matchId : '', templateId : '', teamA : '', teamB : '', templateTime : '',templateList : [], matchList : [], startTime : ''};
            var messageData = {showAlert : false, messageClass : '',  message : '', };
            that.defaultMsg = angular.copy(messageData);          
            that.result =angular.copy(defaultData);
            var getTourAndMatchData = function () {
                that.defaultMsg = angular.copy(messageData);
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response) { 
                    if (response.respCode == 100) {
                        for (var i = 0; i < response.respData.length; i++) {
                            if (response.respData[i].status != 'PENDING') {
                                that.tourList.push(response.respData[i]);
                            }
                        }
                    }
                    else {
                        that.defaultMsg.showAlert = true;
                        that.defaultMsg.messageClass = 'alert-danger';
                        that.defaultMsg.message = response.message;
                    }
                    
                });               
            };
            getTourAndMatchData();

            that.getMatchList = function () {
                that.result = angular.copy(defaultData);
                that.defaultMsg = angular.copy(messageData);
                var data = {
                    tourId: that.tourId,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_LIST, data, function (response){
                    if (response.respCode == 100) {
                        angular.forEach(response.respData, function (value, key) {
                            if (value.status == 'ACTIVE')
                                that.result.matchList.push(value);
                        });
                    } else {
                        that.defaultMsg.showAlert = true;
                        that.defaultMsg.messageClass = 'alert-danger';
                        that.defaultMsg.message = response.message;
                    }

                });
            };
            
            var getTime = function(time){
                time = time * 1000;
                time  = new Date(time);
                return time.toDateString();
            }

            that.getMatchTemplateList = function (matchData) {
                that.templateList = [];
                that.defaultMsg = angular.copy(messageData);
                matchData = JSON.parse(matchData);
                that.result.teamA = matchData.teams.a.name;
                that.result.teamB = matchData.teams.b.name;
                that.result.startTime = matchData.startTime;
                that.result.matchId = matchData.matchId;    
                that.result.startTime = getTime(matchData.startTime);
                var data = {
                    tourId: matchData.tourId,
                    matchId: that.result.matchId,
                    isDefaultLeague : false
                };               
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.LEAGUE_TEMPLATE_LIST, data, function (response){
                    if (response.respCode == 100) {
                       that.result.templateList = response.respData;
                       
                    } else {
                        that.showAlert = true;
                        that.defaultMsg.showAlert = true;
                        that.defaultMsg.messageClass = 'alert-danger';
                        that.defaultMsg.message = response.message;
                    }
                });
            };

            that.onCreateLeague = function (item) {
                that.defaultMsg = angular.copy(messageData);
                var data = {
                    tourId: that.tourId,
                    matchId:  that.result.matchId,
                    templateId: item.templateId ,
                    templateTime : item.time,
                }
                if(item.time =='' || item.time == undefined){
                     that.defaultMsg.showAlert = true;
                        that.defaultMsg.messageClass = 'alert-danger';
                        that.defaultMsg.message = msgConstants.ERR_START_TIME_BLANK;
                    return false;
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.CREATE_SPECIAL_LEG, data, function (response){
                    if (response.respCode == 100) {
                        that.defaultMsg = angular.copy(messageData);    
                        item.templateId = '';
                        item.time = '';
                        that.defaultMsg.showAlert = true;
                        that.defaultMsg.messageClass = 'alert-success';
                        that.defaultMsg.message = msgConstants.CREATE_LEAGUE_SUCCESS;
                    } else {
                        that.defaultMsg = angular.copy(messageData);   
                        that.defaultMsg = angular.copy(errorData); 
                        that.defaultMsg.showAlert = true;
                        that.defaultMsg.messageClass = 'alert-danger';
                        that.defaultMsg.message = response.message;
                    }
                });
            };
            
            that.onCloseAlert = function () {
                that.defaultMsg.showAlert = false;
            };

            
                       
            
        });
