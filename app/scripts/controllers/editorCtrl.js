(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('editorCtrl', function ($scope, $http, ngDialog, $timeout, Upload, myConfig, $rootScope, serverApi, UtilityService, msgConstants) {
                var that = this;
                that.showAlert = false;
                that.isExist = false;
                that.pageNameWarning = '';
                that.selectPageWarning = '';
                that.showTemplateList = true;
                that.userAction = 'LIST_TEMPLATE';
                that.showSaveBtn = false;

                var defaultData = {metaDescription: '', metaKeywords: '', metaTitle: '', pageDispName: '', urlName: '',
                    pageContent: '', pageTitle: '', pageId: '', status: '', typeId: '-1'};
                var defaultErroMsg = {metaDescriptionError: '', metaKeywordsError: '', metaTitleError: '', pageContentError: '',
                    pageDispNameError: '', urlNameError: '',pageTitleError : ''};
                that.result = angular.copy(defaultData);
                that.errorMsg = angular.copy(defaultErroMsg);
                that.editorOptions = {
                    language: 'en',
                    height: 320,
                    width: '100%'
                };

                that.addCmsTemplate = function () {
                    that.showTemplateList = false;
                    that.userAction = 'ADD_TEMPLATE';
                    that.showSaveBtn = true;
                    that.errorMsg = angular.copy(defaultErroMsg);
                    that.result = angular.copy(defaultData);
                }

                that.listCmsTemplates = function () {
                    that.showTemplateList = true;
                    that.userAction = 'LIST_TEMPLATE';
                }

                var reqData = {};
                // Function to Get Template List.
                UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CMS_LIST_GET, reqData, function (response) {    
                    if (response.respCode == 100) {
                        that.cmsTemplatesList = response.respData;
                    }
                });
                
                UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CMS_PAGE_TYPE_LIST, reqData, function (response) {    
                    if (response.respCode == 100) {
                        that.templateTypeList = response.respData;
                    }
                });

                //Form Validation Check.

                var validateForm = function () {
                    that.showAlert = false;
                    that.errorMsg = angular.copy(defaultErroMsg);
                    var isSuccess = true;
                    
                    if (that.result.pageDispName == '') {
                        that.errorMsg.pageDispNameError = msgConstants.ERR_CMS_DISP_NAME;
                        isSuccess = false;
                        return isSuccess;
                    }
                    
                    if (that.result.pageTitle == '') {
                        that.errorMsg.pageTitleError = msgConstants.ERR_CMS_TITLE_NAME;
                        isSuccess = false;
                         return isSuccess;
                    }

                    if (that.result.urlName == '') {
                        that.errorMsg.urlNameError = msgConstants.ERR_CMS__URL;
                        isSuccess = false;
                         return isSuccess;
                    }

                    
                    if (that.result.metaDescription == '' || that.result.metaDescription  == null) {
                        that.errorMsg.metaDescriptionError = msgConstants.ERR_CMS_DISCRIP;
                        isSuccess = false;
                        return isSuccess;
                    }
                    if (that.result.metaKeywords == '') {
                        that.errorMsg.metaKeywordsError = msgConstants.ERR_CMS_META_KEY;
                        isSuccess = false;
                         return isSuccess;
                    }

                    if (that.result.metaTitle == '') {
                        that.errorMsg.metaTitleError = msgConstants.ERR_CMS_META_TITLE;
                        isSuccess = false;
                         return isSuccess;
                    }

                    if (that.result.pageContent == '') {
                        that.errorMsg.pageContentError = msgConstants.ERR_CMS_PAGE_CONTENT;
                        isSuccess = false;
                        return isSuccess;
                    }
                    return isSuccess;
                }

                // Funtion for load data of CMS page for selected page category.
                that.loadCMSPageData = function (pageId) {
                    that.isExist = false;
                    that.showAlert = false;
                    that.selectPageWarning = '';
                    that.showSaveBtn = false;
                    var data = {
                        pageId: pageId,
                    };
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CMS_PAGE_DATA, data, function (response) {
                            if (response.respCode == 100) {
                                var responseCmsObj = response.respData;
                                if (response.respData != null) {
                                    that.showTemplateList = false;
                                    that.isExist = true;
                                    that.result.metaDescription = responseCmsObj.metaDescription;
                                    that.result.metaKeywords = responseCmsObj.metaKeywords;
                                    that.result.metaTitle = responseCmsObj.metaTitle;
                                    that.result.pageContent = responseCmsObj.pageContent;
                                    that.result.pageDispName = responseCmsObj.pageDispName;
                                    that.result.pageTitle = responseCmsObj.pageTitle;
                                    that.result.pageId = responseCmsObj.pageId;
                                    that.result.status = responseCmsObj.status;
                                    that.result.typeId = responseCmsObj.typeId + '';
                                    that.result.urlName = responseCmsObj.urlName;
                                }
                            }
                             else{
                                that.result = angular.copy(defaultData);
                                that.result.pageDevName = pageDevName;
                            }

                        });
                    

                };

                that.getPageTypeId = function () {
                    var data = {
                        pageId: that.pageId,
                    }
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CMS_PAGE_TYPE_LIST, data, function (response) {
                        console.log("Resp:::::",response);
                    })
                }


                // To Submit form Data for CMS page.
                that.onSubmitFormData = function () {
                    that.showAlert = false;
                    that.selectPageWarning = '';
                    that.pageNameWarning = '';
                    var data = {
                        metaDescription: that.result.metaDescription,
                        metaKeywords: that.result.metaKeywords,
                        metaTitle: that.result.metaTitle,
                        pageContent: that.result.pageContent,
                        pageDispName: that.result.pageDispName,
                        urlName: that.result.urlName,
                        pageTitle: that.result.pageTitle,
                        status: that.result.status,
                        typeId: that.result.typeId,
                        token : localStorage.adminToken,
                    };
                    
                    
                     if (validateForm()) {
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CMS_PAGE_SAVE, data, function (response) {
                            if (response.respCode === 100) {
                                that.isExist = false;
                                that.resetData();
                                that.showAlert = true;
                                that.messageClass = 'alert-success';
                                that.message = msgConstants.CMS_SUCCESS;
                            } else {
                                that.showAlert = true;
                                that.messageClass = 'alert-danger';
                                that.message =  msgConstants.ERR_SERVER;
                            }
                        });
                    }
                    window.scrollTo(0, 0);
                };

                // To Update form Data for CMS page.
                that.onUpdateFormData = function () {
                    that.showAlert = false;
                    that.selectPageWarning = '';
                    that.pageNameWarning = '';
                    var data = {
                        metaDescription: that.result.metaDescription,
                        metaKeywords: that.result.metaKeywords,
                        metaTitle: that.result.metaTitle,
                        pageContent: that.result.pageContent,
                        pageDispName: that.result.pageDispName,
                        urlName: that.result.urlName,
                        pageTitle: that.result.pageTitle,
                        pageId: that.result.pageId,
                        status: that.result.status,
                        typeId: that.result.typeId,
                        token : localStorage.adminToken,
                    };
                    
                    if (validateForm()) {
                        $rootScope.$emit('showloader');
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.CMS_PAGE_UPDATE, data, function (response){
                            $rootScope.$emit('hideloader');
                            if (response.respCode === 100) {
                                that.isExist = false;
                                that.showAlert = true;
                                that.resetData();
                                that.messageClass = 'alert-success';
                                that.message = msgConstants.CMS_UPDATE;
                            } else {
                                that.showAlert = true;
                                that.messageClass = 'alert-danger';
                                that.message = response.message;
                            }
                        });
                    }
                    window.scrollTo(0, 0);
                };

                //close Alert box message
                that.onCloseAlert = function () {
                    that.showAlert = false;
                };

                that.resetData = function () {
                    that.result.metaDescription = '';
                    that.result.metaKeywords = '';
                    that.result.metaTitle = '';
                    that.result.pageContent = '';
                    that.result.pageDispName = '';
                    that.result.urlName = '';
                    that.result.pageTitle = '';
                    that.result.pageId = '';
                    that.result.status = 'ACTIVE';
                    that.result.typeId = 1;
                }

                //iframe function
                that.iframeLoadedCallBack = function () {
                    // do stuff
                    var cssLink = '<link rel="stylesheet" type="text/css" href="styles/teenpatti-template.css">';
                    var baseTag = '<base href="' + myConfig.websiteUrl + '" target="_blank">';
                    var pageData = '';
                    $('.modal-body-editor iframe').contents().find('head').append(cssLink);
                    $('.modal-body-editor iframe').contents().find('head').append(baseTag);
                    // Removing STATIC_URL from image links
                    pageData = that.result.pageContent;
                    pageData = pageData.replace(/STATIC_URL/g, '');

                    $('.modal-body-editor iframe').contents().find('body').addClass('dynamic-page-container');
                    $('.modal-body-editor iframe').contents().find('body').html(pageData);
                };

                that.preview = function () {
                    ngDialog.open({
                        template: "views/editorTemplate.html",
                        plain: false,
                        scope: $scope,
                        overlay: true,
                        closeByDocument: true,
                        closeByEscape: true,
                        closeByNavigation: true,
                        appendTo: 'body',
                        disableAnimation: false,
                        showClose: true,
                        className: 'ngdialog ngdialog-theme-default'
                    });
                };
            })
            .directive('iframeOnload', [function () {
                    return {
                        scope: {
                            callBack: '&iframeOnload'
                        },
                        link: function (scope, element, attrs) {
                            element.on('load', function () {
                                return scope.callBack();
                            });
                        }
                    };
                }]);
})();