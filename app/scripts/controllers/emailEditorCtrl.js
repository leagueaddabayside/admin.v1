(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('emailEditorCtrl', ['$scope', '$http', 'ngDialog', '$timeout', 'Upload', 'UtilityService',
                'myConfig', 'msgConstants', 'serverApi', function ($scope, $http, ngDialog, $timeout, Upload,
                        UtilityService, myConfig, msgConstants, serverApi) {
                    var that = this;
                    var defaultData = {templateType : '',emailName: '',emailSubject: '', emailBody: '',  templateId: '',emailTemplatesArray:[]};                  
                    var defaultError = {templateTypeError:'', emailNameError:'', emailSubjectError:'', emailBodyError:''}
                    
                    that.result = angular.copy(defaultData);
                    that.errorData = angular.copy(defaultError);
 
                    that.showAlert = false;
                    that.isExist = false;
                    that.userAction = '';
                    that.message = '';
                    that.showTemplateList = true;
                    that.emailTemplatesArray = [];

                    that.editorOptions = {
                        language: 'en',
                        height: 320,
                        width: '100%'
                    };

                    //Dafult JSON name value pair for page catgory.
                    $scope.$on("ckeditor.ready", function (event) {
                        that.isReady = true;
                    });

                    that.listEmailTemplates = function () {
                        var data = {};
                        that.showAlert = false;
                        that.userAction = "LIST_TEMPLATE";
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.EMAIL_LIST_GET, data, function (response) {    
                            if (response.respCode == 100) {
                                that.emailTemplatesArray = response.respData;
                            }
                            else{
                               that.showAlert = true;
                               that.messageClass = 'alert-danger';
                               that.message = response.message; 
                            }
                        });
                    }

                    that.loadEmailTemplate = function (id) {
                        var data = {
                            templateId: id,
                        }
                        that.isExist = true;
                        that.userAction = "EDIT_TEMPLATE";
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.EMAIL_FIND_BY_PROP, data, function (response) {
                            if (response.respCode == 100) {
                                that.result = response.respData;
                                that.showTemplateList = false;
                            }
                            else{
                               that.showAlert = true;
                               that.messageClass = 'alert-danger';
                               that.message = response.message; 
                            }
                        });
                    }

                    that.addEmailTemplate = function () {
                        that.isExist = false;
                        that.showAlert = false;
                        that.userAction = "ADD_TEMPLATE";
                        that.showTemplateList = false;
                        that.result = angular.copy(defaultData);
                        that.errorData = angular.copy(defaultError);
                        $('.form-group').show();
                    }

                    var validateForm = function () {
                        that.showAlert = false;
                        that.errorData = angular.copy(defaultError);
                        var isSuccess = true;
                        if (that.result.templateType == '') {
                            that.errorData.templateTypeError = msgConstants.ERR_EMAIL_TEMPLATE;
                            isSuccess = false;
                            return isSuccess;
                        }
                        if (that.result.emailName == '') {
                            that.errorData.emailNameError = msgConstants.ERR_EMAIL_NAME;
                            isSuccess = false;
                            return isSuccess;
                        }

                        if (that.result.emailSubject == '') {
                            that.errorData.emailSubjectError = msgConstants.ERR_EMAIL_SUB;
                            isSuccess = false;
                            return isSuccess;
                        }

                        if (that.result.emailBody == '') {
                            that.errorData.emailBodyError = msgConstants.ERR_EMAIL_CONTENT;
                            isSuccess = false;
                            return isSuccess;
                        }
                        return isSuccess;
                    }

                    that.saveEmailTemplate = function () {
                        var data = {
                            templateType: that.result.templateType,
                            emailName: that.result.emailName,
                            emailSubject: that.result.emailSubject,
                            emailBody: that.result.emailBody,
                            status: 'ACTIVE',
                        }
                        if (validateForm()) {
                            UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.EMAIL_TEMPL_SAVE, data, function (response) {
                                if (response.respCode == 100) {
                                    that.showAlert = true;
                                    that.messageClass = 'alert-success';
                                    that.message = msgConstants.TEMPLATE_SAVE;
                                    that.result = angular.copy(defaultData);
                                    that.errorData = angular.copy(defaultError);
                                } else {
                                    that.showAlert = true;
                                    that.messageClass = 'alert-danger';
                                    that.message = response.message;
                                }
                            });
                        }
                        window.scrollTo(0, 0);
                    }

                    that.updateEmailTemplate = function () {
                        var data = {
                            templateId: that.result.templateId,
                            templateType: that.result.templateType,
                            emailName: that.result.emailName,
                            emailSubject: that.result.emailSubject,
                            emailBody: that.result.emailBody,
                            status: 'ACTIVE',
                            token : localStorage.adminToken,
                        }
                        if (validateForm()) {
                            UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.EMAIL_TEMPL_UPDATE, data, function (response) {
                                if (response.respCode == 100) {
                                    that.showAlert = true;
                                    that.messageClass = 'alert-success';
                                    that.message = msgConstants.TEMPLATE_UPDATE;
                                    that.result = angular.copy(defaultData);
                                    that.errorData = angular.copy(defaultError);
                                    that.isExist = false;
                                } else {
                                    that.showAlert = true;
                                    that.messageClass = 'alert-danger';
                                    that.message = response.message;
                                }
                            });
                        }
                        window.scrollTo(0, 0);
                        // $('.form-group').hide();
                    }

                    //close Alert box message
                    that.onCloseAlert = function () {
                        that.showAlert = false;
                    };

                    // function call to load email templates on page load
                    that.listEmailTemplates();


                }]);
})();