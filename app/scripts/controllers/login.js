(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('LoginCtrl', function ($scope,  $location,  $rootScope, UtilityService, msgConstants, myConfig, serverApi ) {
                    var that = this;
                    var defaultData ={username : '',password : ''};
                    that.result = angular.copy(defaultData);
                    that.onLoginClick = function () {
                    var reqData = that.result;
                    var validateData = {};
                    if (reqData.userName !== "" & reqData.password !== "") {
                        reqData.password = md5(reqData.password);
                        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.LOGIN_REQ, reqData, function (response) {
                            if (response.respCode === 100) {
                                localStorage.adminToken = response.respData.token;
                                validateData.token = response.respData.token;
                                UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.VALIDATE_TOKEN, validateData, function (validateRespData){
                                    if(validateRespData.respCode === 100){
                                        var respData = validateRespData.respData;
                                        that.adminName = respData.userName;
                                        $rootScope.userId = respData.userId;
                                        localStorage.adminToken = respData.token;
                                        $rootScope.loginSuccess = true;
                                        var data={
                                            userId : $rootScope.userId,
                                        };
                                        $location.path('dashboard');
                                    }else{
                                        reqData.password = "";
                                        that.message = validateRespData.message;
                                    }

                                });
                            }
                            else {
                                reqData.password = ""; 
                                that.message = response.message;
                            }
                        });
                    }
                    else {
                        reqData.password = "";
                        that.message = msgConstants.ERR_USER_LOGIN;
                    }
                };

            });
})();

