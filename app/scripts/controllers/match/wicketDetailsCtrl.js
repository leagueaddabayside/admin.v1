'use strict';
angular.module('adminFantasyApp')
        .controller('wicketDetailsCtrl', function ($scope, allTourService, $location, $rootScope ) {
            var that = this;
            angular.element(document).ready(function () {
                allTourService.fetchToursList(function (response) {
                    that.tourList = response.respData;
                });               
            })
            
            that.getMatchList= function(){
                var data = {
                    tourId : that.tourId,
                    
                }
                allTourService.fetchAllMatchList(function (response) {
                    if(response.respCode ==100){
                      that.matchlist = response.respData;  
                    }
                    else{
                      that.alert = "Select Corrent Tour";  
                    }
                    
                });
            }
            
            that.getPLayerList= function(){
                var data = {
                    tourId : that.tourId,
                    matchId : that.tourId,
                    teamId : that.teamId,
                }
                allTourService.fetchMatchPlayerList(data, function (response) {
                    if(response.respCode ==100){
                      that.playerList = response.respData;  
                    }
                    else{
                      that.alert = "Server Down";  
                    }
                    
                });
            }
            
            that.getPalyerWicketInfo= function(){
                var data = {
                    tourId : that.tourId,
                    matchId : that.tourId,
                    teamId : that.teamId,
                    playerId : that.playerId,
                }
                allTourService.fetchPlayerWicketsData(data, function (response) {
                    if(response.respCode ==100){
                      that.playerMatchInfo = response.respData;  
                    }
                    else{
                      that.alert = "Server Down";  
                    }
                    
                });
            }
            
            that.onSubmitPlayerWicketInfo= function(){
                var data = {
                    catch : that.tourId,
                    stump : that.tourId,
                    runOut : that.teamId,
                    runoutThrower : that.playerId,
                    runOutStump : that.teamId,
                    caughtBold : that.playerId,
                }
                allTourService.updatePlayerWicketInfo(data, function (response) {
                    if(response.respCode ==100){
                      that.playerMatchInfo = response.respData;  
                    }
                    else{
                      that.alert = "Server Down";  
                    }
                    
                });
            }
    
    
    
    
    

        });
