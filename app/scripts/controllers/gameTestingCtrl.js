(function () {
    'use strict';
    angular.module('adminFantasyApp')
            .controller('gameTestingCtrl', ['$scope', 'UtilityService', 'serverApi', 'myConfig', 'gameJsonData', function ($scope, UtilityService, serverApi, myConfig, gameJsonData) {
                    var that = this;
                    that.matchTypeList = gameJsonData.matchTypeList;
                    that.simulatorData = {};
            that.resetGameTestingData = function(){
                that.templateId = '-1';
                that.matchType = '-1';
                that.templateName = '';
                that.gameScoreData = {};

            }

            that.resetExceptTemplateId = function(){
                that.matchType = '-1';
                that.templateName = '';
                that.gameScoreData = {};

            }
                    that.getTemplateList = function(){
                        UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.GAME_SCORE_TEMPLATE_LIST, {}, function (response) {
                        console.log("Template List", response);
                        if (response.respCode == 100) {
                            that.gameScoreTemplateList = response.respData;
                        } else {
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                        }
                    });
                    }

                    that.getTemplateList();
                    that.resetGameTestingData();


                    that.boolToStr = function (arg) {
                        return arg ? 'true' : 'false'
                    };

            that.onChangeMatchType = function () {
                var matchType = that.matchType;
                if(matchType == 'T20'){
                    that.gameScoreData = gameJsonData.T20Match;
                }else if(matchType == 'TEST'){
                    that.gameScoreData = gameJsonData.TestMatch;
                }else if(matchType == 'ODI'){
                    that.gameScoreData = gameJsonData.ODIMatch;
                }
            }
                    that.onChangeScoreTemplate = function () {
                        var templateId = that.templateId;
                        var data = {};
                        if(templateId == -1){
                            //reset data
                            that.resetGameTestingData();
                        }else{
                            data.templateId = templateId;

                            UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.SCORE_TEMP_DATA_BY_ID, data, function (response) {
                                console.log("Template Data", response);
                                if (response.respCode == 100) {
                                    var respData = response.respData;

                                    if(respData.templateName == 'SIMULATOR_DATA'){
                                      that.resetExceptTemplateId();
                                      that.simulatorData = JSON.stringify(respData.data);
                                    }else{
                                        that.matchType = respData.data.matchType;
                                        that.templateName = respData.templateName;
                                        that.gameScoreData = respData.data.gameScoreData;
                                    }

                                    //update data with server data

                                } else {

                                }
                            });

                        }

                    }




//                    that.onGamePointsSave = function () {
//                        var data = gameJsonData.T20Match;
//                        data.templateName = that.templateName;
//                        console.log("JSON to Server :: ", that.matchType);
//                        if (that.matchType == 'T20') {
//                            data = gameJsonData.T20Match;
//                        } else if (that.matchType == 'ODI') {
//                            data = gameJsonData.ODIMatch;
//                        } else if (that.matchType == 'TEST') {
//                            data = gameJsonData.TestMatch;
//                        }
//                        data.players = that.matchPlayerList;
//                        console.log("Match PLayer Data", data);
//                        UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.ADD_GAME_SCORE, data, function (response) {
//                            console.log("Template Data", response);
//                            if (response.respCode == 100) {
//                                that.gameScoreTemplateList = response.respData;
//                            } else {
//                                that.showAlert = true;
//                                that.message = response.message;
//                                that.messageClass = 'alert-danger';
//                            }
//                        });
//                    }

                    that.onGamePointsAction = function () {
                        var scoreTemplateData = {};

                        console.log("Match PLayer Data", data);
                        if (that.templateId == '-1') {
                            var data = {};
                            data.matchType = that.matchType;
                            scoreTemplateData.templateName = that.templateName;
                            data.gameScoreData = that.gameScoreData;
                            scoreTemplateData.data = data;


                            console.log("ADD New Template",scoreTemplateData);
                            UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.ADD_GAME_SCORE, scoreTemplateData, function (response) {
                                console.log("Template Data", response);
                                if (response.respCode == 100) {
                                    that.getTemplateList();
                                 that.resetGameTestingData();
                                } else {

                                }
                            });
                        }
                        else {
                            scoreTemplateData.templateId = that.templateId;
                            var data = {};
                            if(that.templateId == 1){
                              scoreTemplateData.data = JSON.parse(that.simulatorData);
                            }
                            else{
                              data.matchType = that.matchType;
                              scoreTemplateData.templateName = that.templateName;
                              data.gameScoreData = that.gameScoreData;
                              scoreTemplateData.data = data;
                            }


                            console.log("UPDATE Template",scoreTemplateData);
                            UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.UPDATE_GAME_SCORE, scoreTemplateData, function (response) {
                                console.log("Template Data", response);
                                if (response.respCode == 100) {
                                    that.resetGameTestingData();
                                } else {

                                }
                            });
                        }


                    }

                    //                    that.calcPlayerPoints = function(item){
//                         that.totalPoints = that.battingPoints(item) + that.bowlingPoints(item) + that.fieldingPoints(item);
//                    }
//
//                    that.battingPoints = function(item){
//                        return  item.match.innings['1'].batting.runs * matchTypePoints.gameType.runs +
//                                    item.match.innings['1'].batting.fours *  matchTypePoints.gameType.runs+
//                                    item.match.innings['1'].batting.six * matchTypePoints.gameType.runs +
//                                    item.match.innings['1'].batting.sr  + matchTypePoints.gameType.runs
//                    }
//
//                    that.bowlingPOints = function(item){
//                         return  item.match.innings['1'].batting.wickets * matchTypePoints.gameType.wickets +
//                                    item.match.innings['1'].batting.econ *  matchTypePoints.gameType.econ+
//                    }
//
//                    that.fieldingPoints = function(item){
//                         return  item.match.innings['1'].batting.catch * matchTypePoints.gameType.catch +
//                                    item.match.innings['1'].batting.runout *  matchTypePoints.gameType.runout+
//                                    item.match.innings['1'].batting.runout * matchTypePoints.gameType.runout;
//                    }
                    //                    that.calcPlayerPoints = function(item){
//                         that.totalPoints = that.battingPoints(item) + that.bowlingPoints(item) + that.fieldingPoints(item);
//                    }
//
//                    that.battingPoints = function(item){
//                        return  item.match.innings['1'].batting.runs * matchTypePoints.gameType.runs +
//                                    item.match.innings['1'].batting.fours *  matchTypePoints.gameType.runs+
//                                    item.match.innings['1'].batting.six * matchTypePoints.gameType.runs +
//                                    item.match.innings['1'].batting.sr  + matchTypePoints.gameType.runs
//                    }
//
//                    that.bowlingPOints = function(item){
//                         return  item.match.innings['1'].batting.wickets * matchTypePoints.gameType.wickets +
//                                    item.match.innings['1'].batting.econ *  matchTypePoints.gameType.econ+
//                    }
//
//                    that.fieldingPoints = function(item){
//                         return  item.match.innings['1'].batting.catch * matchTypePoints.gameType.catch +
//                                    item.match.innings['1'].batting.runout *  matchTypePoints.gameType.runout+
//                                    item.match.innings['1'].batting.runout * matchTypePoints.gameType.runout;
//                    }


                }]);
})();
