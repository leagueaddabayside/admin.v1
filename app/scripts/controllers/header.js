(function () {
    'use strict';
    // To disable history in browser. Back button feature disable.
	window.addEventListener('popstate', function(e) {
        history.pushState(null, null, '');
    });

    angular.module('adminFantasyApp')
            .controller('headerCtrl', function ($scope,  UtilityService, $location, $rootScope, ngDialog, serverApi, myConfig) {
                var that = this;
                var data = {};
                //$rootScope.loginSuccess = false;
                that.showHeaderMenu = false;
                data.token = localStorage.adminToken;
                if($rootScope.loginSuccess == true){
                    that.showHeaderMenu = true;
                }
                if (data.token && data.token != '') {
                     UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.VALIDATE_TOKEN, data, function (tokenResponseData) {
                         var respData = tokenResponseData.respData;
                        if (tokenResponseData.respCode == 100) {
                            that.adminName = respData.screenName;
                            that.showHeaderMenu = true;
                            $rootScope.userId = respData.userId;
                            $rootScope.loginSuccess = true;
                            $location.path('dashboard');
                        }
                        else if(tokenResponseData.respCode == 108){
                            that.showHeaderMenu = true;
                            $rootScope.loginSuccess = true;
                            $rootScope.userId = respData.userId;
                            $location.path('dashboard');
                        }
                        else {
                            if(respData && respData.message){
                                that.message = respData.message;
                            }

                            $rootScope.loginSuccess = false;
                            $location.path('login');
                        }
                    });

                }else{
                    $location.path('login');
                }
                that.userLogOut = function(){
                    var data = {};
                    data = {
                       token : localStorage.adminToken,
                    };
                    localStorage.removeItem('adminToken');
                    UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.LOGOUT_REQ, data, function (response){
                        if (response.respCode == 100) {
                           $rootScope.loginSuccess = false;
                           that.showHeaderMenu = false;
                           $location.path('login');
                        }
                        else{
                            $rootScope.loginSuccess = false;
                            that.showHeaderMenu = false;
                            $location.path('login');

                        }
                    });
                };

//                that.showDashboard = function(){
//                    $rootScope.isMenuSelected = -1;
//
//                }
            });
    })();
