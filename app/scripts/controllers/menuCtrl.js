(function () {
    'use strict';

  angular.module('adminFantasyApp')
    .controller('admnMenuCtrl', function ($scope, $rootScope, UtilityService, $timeout, myConfig, serverApi) {
        var that = this;
        $rootScope.isMenuSelected = -1;

        $rootScope.menuSlide = {
            isSlide: true,
            menuToggle: true
        };

        var reqData = {
            userId: $rootScope.userId,
        };
        UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.SIDE_MENU_LIST, reqData, function (response){
            that.menuCollection = response.respData;
        });

        that.onMenuSelect = function (index) {
            $rootScope.isMenuSelected = index;
        };

        that.menuToggle = function(){
        };
        
        
        $rootScope.$on('showloader', function (event) {
          var x = document.getElementById('loader').classList;
          x.remove('hide-loader');
          x.add('show-loader');
        });

        $rootScope.$on('hideloader', function (event) {
            var x = document.getElementById('loader').classList;
            x.remove('show-loader');
            x.add('hide-loader');

        });

    });
})();