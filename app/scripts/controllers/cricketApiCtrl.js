'use strict';
angular.module('adminFantasyApp')
        .controller('cricketApiCtrl', function ($scope, $rootScope, UtilityService, ngDialog, msgConstants, serverApi, myConfig) {
            var that = this;   
            that.isDataError = false;
            that.showAlert = false;
            that.message = "";
            that.messageClass = '';
            that.updateTour = function(){
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_UPDATE, data, function (response)  {
                        if (response.respCode == 100) {  
                        that.showAlert = true;
                        that.message = msgConstants.SUCCESS_TOUR_UPDATE;
                        that.messageClass = 'alert-success';                           
                        }
                        else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                        }
                    });
            }
            
            that.updatePlayer = function(){
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.PLAYER_LIST_UPLOAD, data, function (response)  {
                        if (response.respCode == 100) {  
                        that.showAlert = true;
                        that.message = msgConstants.SUCCESS_TOUR_PLAYER;
                        that.messageClass = 'alert-success';                           
                        }
                        else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                        }
                    });
            },
            
            that.updateMatch = function(){
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                var data = {};
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST_UPLOAD, data, function (response)  {
                        if (response.respCode == 100) {  
                        that.showAlert = true;
                        that.message = msgConstants.SUCCESS_TOUR_MATCH;
                        that.messageClass = 'alert-success';                           
                        }
                        else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                        }
                    });
            }
            
            that.getchAllTourList = function () {
                that.tourId = '',   
                that.matchId = '';
                that.matchlist = '';
                that.selected = '';
                that.allToursList = [];
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                var data = {}; 
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.TOUR_LIST, data, function (response) {
                    if (response.respCode == 100) {
                        that.activeTourList(response);
                    } else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }
                });
            } 
            
            that.activeTourList = function(data){
                for(var i =0; i < data.respData.length; i++){
                        if(data.respData[i].status != 'PENDING'){
                            that.allToursList.push(data.respData[i]);
                        }                       
                    }
            }
            
            that.getMatchList= function(data){
                that.tourId = data.tourId;
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                var data = {
                    tourId : data.tourId,  
                    token : localStorage.adminToken,
                }
                UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_LIST, data, function (response)  {
                    if(response.respCode ==100){
                      that.matchlist = response.respData;  
                    }
                    else {
                        that.showAlert = true;
                        that.message = response.message;
                        that.messageClass = 'alert-danger';
                    }                    
                });
            }
            
            that.updateMatchPoints = function(){
                var isValid = true;
                that.showAlert = false;
                that.message = "";
                that.messageClass = '';
                var data = {
                    tourId : that.tourId,   
                    matchId : that.matchId,
                    token : localStorage.adminToken,
                }
                if(data.matchId == ''){
                    isValid = false;
                    that.showAlert = true;
                    that.message = msgConstants.ERR_MATCH_NOT_SELECT;
                    that.messageClass = 'alert-danger';
                    ngDialog.close();
                    return;
                }
                if(isValid && data.tourId == ''){
                    isValid = false;
                    that.showAlert = true;
                    that.message = msgConstants.ERR_TOUR_NOT_SELECT;
                    that.messageClass = 'alert-danger';
                    ngDialog.close();
                    return;
                }
                if(isValid){
                   UtilityService._postAjaxCall(myConfig.fantasyUrl + serverApi.MATCH_POINTS_UPDATE, data, function (response)  { 
                    if (response.respCode == 100) {
                            ngDialog.close();
                            that.showAlert = true;
                            that.messageClass = 'alert-success';
                            that.message = msgConstants.SUCCESS_MATCH_POINTS;                          
                            return;
                        } else {
                            ngDialog.close();
                            that.showAlert = true;
                            that.message = response.message;
                            that.messageClass = 'alert-danger';
                            return;
                        }                    
                }); 
                }
                
            }
            
            that.onCloseAlert = function () {
                that.showAlert = false;
            };
                        
            
        });
