'use strict';

angular.module('adminFantasyApp')
        .directive('editListData', function (ngDialog, $timeout) {
            return {
                restrict: 'C',
                link: function postLink(scope, element, attr) {
                    element.bind('click', function () {  
                        var templateName = '';
                        function handelActiveMenu() {
                            var currentElem = document.getElementById(attr.dataid);
                            var activeElemArr = document.querySelectorAll('.active-row');
                            for (var i = 0; i < activeElemArr.length; i++) {
                                activeElemArr[i].classList.remove('active-row');
                            }
                            currentElem.className += ' active-row';
                        }
                       
                       // handelActiveMenu();
                        scope.$apply(function () {
                            var currentPage = attr.templatename;                            
                            switch (currentPage) {
                                case 'edit-match-detail' :
                                    templateName = 'views/popups/editMatchDetails.html';
                                    break;
                                case  'edit-player-squad' :
                                    templateName = 'views/popups/editSquadPlayer.html';
                                    break;
                                case  'edit-tour-detail':
                                    templateName = 'views/popups/editTours.html';
                                    break;
                                case  'wicket-detail-update':
                                    templateName = 'views/popups/wicketdetailsupdate.html';
                                    break;
                                case 'pancard-detail-update':
                                    templateName = 'views/popups/editPancardDetail.html';
                                    break;
                                case 'admin-compliance-request':
                                    templateName = 'views/popups/adminComplianceApprove.html';
                                    break; 
                                case 'update-match-points':
                                    templateName = 'views/popups/updateMatchPoints.html';
                                    break;
                                case 'document-detail-update':
                                    templateName = 'views/popups/editUserDocumentDetail.html';
                                    break;
                                case 'withdrawal-request-accounts':
                                    templateName = 'views/popups/withdrlApproveAccounts.html';
                                    break;
                            }
                            ngDialog.open({
                                template: templateName,
                                scope: scope,
                                overlay: true,
                                closeByDocument: false,
                                closeByEscape: false,
                                showClose: false,
                                closeByNavigation: true,
                                disableAnimation: true,
                                appendTo: '#' + 'admincontainer', //scope.game.parsedCurrRoomName,
                                className: 'ngdialog-theme-popup',
                            }).closePromise.then(function () {
                            });                                    
                        });
                        

                    });
                }
            };
        });
