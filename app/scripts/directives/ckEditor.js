/**
 * Created by sumit on 11/24/2016.
 */
angular.module('adminFantasyApp').directive('ckEditor', function ($timeout) {
  return {
    require: '?ngModel',
    link: function (scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0],{
        customConfig: 'config.js'
      });


      if (!ngModel) return;
      ck.on('instanceReady', function () {
        ck.setData(ngModel.$viewValue);
      });
      function updateModel() {

        $timeout(function() {
          ngModel.$setViewValue(ck.getData());
        })

/*        scope.$apply(function () {
          ngModel.$setViewValue(ck.getData());
        });*/
      }
      ck.on('change', updateModel);
      ck.on('key', updateModel);
      ck.on('dataReady', updateModel);

      ngModel.$render = function (value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  };
});
