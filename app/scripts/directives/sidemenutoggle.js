'use strict';

/**
 * @ngdoc directive
 * @name adminFantasyApp.directive:sidemenuToggle
 * @description
 * # sidemenuToggle
 */
angular.module('adminFantasyApp')
.directive('sidemenuToggle', function($rootScope){
  return{
    restrict: 'C',
    scope: true,
    link: function(scope, element, attrs){

      element.on('click', function(){

        var pageWrapper = document.querySelector('.page-wrapper');
        var mainContent = document.querySelector('.main-content');
        var sideMenu = document.querySelector('.sidemenu');

        // Code to remove no-transition classes on first click
        if(mainContent.classList.contains('no-transition'))
          mainContent.classList.remove('no-transition');

        if(sideMenu.classList.contains('no-transition'))
          sideMenu.classList.remove('no-transition');

        // Code to toggle "menu-open" class on click
        if(pageWrapper.classList.contains('menu-open'))
          pageWrapper.classList.remove('menu-open');
        else
          pageWrapper.classList.add('menu-open');

        // $('.page-wrapper').toggleClass('menu-open');

        // if($('.main-content').hasClass('no-transition'))
        //   $('.main-content').removeClass('no-transition');

        // if($('.sidemenu').hasClass('no-transition'))
        //   $('.sidemenu').removeClass('no-transition');
      })
    }
  }
})