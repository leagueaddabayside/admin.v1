'use strict';
angular.module('adminFantasyApp').service('pagination',
        function ($timeout) {
            var that = this
            that.getPageList = function (data, callback) {
                 var endPoint = data.pageNo * data.pageSize ;
                 var startPoint = endPoint - data.pageSize;
                 if(endPoint > data.recordData.length){
                     endPoint = data.recordData.length;
                 }
                $timeout(function () {
                    for (var i = startPoint; i < endPoint;i++) {
                        data.newPageListData.push(data.recordData[i]);
                    }
                    data.activeClass = 'bg-red';
                    callback(data);
                }, 10);    
            } 
            
            this.setPageSize = function (data, callback) {
                var size = Math.ceil(data.listLength / data.pageSize);
                while (size) {
                    data.pageCount.push(size);
                    size--;
                }
                $timeout(function () {
                    callback(data);
                }, 10);
                
            }
            
              

        });
