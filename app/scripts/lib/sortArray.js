(function () {
    Array.prototype.sortArray = function (key) {
        for (var i = 0; i < this.length; i++) {
            for (var j = 0; j < this.length - i; j++) {
                var temp = '';
                if (this[i][key] > this[j][key]) {
                    temp = this[i];
                    this[i] = this[j];
                    this[j] = temp;
                }
            }
        }
    }
})();