'use strict';
angular.module('adminFantasyApp')
        .constant('serverApi', {
        LOGIN_REQ : '/admin/login',
        VALIDATE_TOKEN : '/admin/validate/token',
        SIDE_MENU_LIST : '/roles/attributes',   
        LOGOUT_REQ : '/admin/logout',
        CHANGE_PASS_REQ : '/admin/change/password',
        PAN_PENDING : '/users/pancard/list',
        PAN_VERIFY : '/users/pancard/approve',
        DOC_PENDING :  '/users/bank/list' ,
        DOC_VERIFY : '/users/bank/approve',
        SERVER_TIME : '/server_time',  
        
        CMS_LIST_GET : '/findCMSList',
        CMS_PAGE_DATA : '/findCMSByProperty',
        CMS_PAGE_TYPE_LIST : '/findCMSPageTypeList',
        CMS_PAGE_SAVE : '/addCMS',
        CMS_PAGE_UPDATE : '/updateCMS',
        
        EMAIL_LIST_GET : '/findEmailTemplateList',
        EMAIL_TEMPL_SAVE : '/addEmailTemplate',
        EMAIL_FIND_BY_PROP: '/findEmailTemplateByProperty',
        EMAIL_TEMPL_UPDATE: '/updateEmailTemplate',
        
        WITHDRAWAL_PENDING : '/payments/withdrawl/history',
        WITHDRAWAL_UPDATE : '/payments/withdrawl/update',
        WITHDRAWAL_APPROVE : '/payments/withdrawl/history/crmapproved',
        
        USER_ACC_INFO : '/payments/users/walletInfo',
        USER_CR_UPDATE : '/payments/credit',
        USER_DR_UPDATE : '/payments/debit',
        
        TOUR_LIST : '/findTourList',
        MATCH_LIST : '/findTourMatchesList',
        UPDATE_TOUR_DATA : '/updateTour',
        
        TOUR_DATA_FETCH : '/findTourByProperty',
        TOUR_DATA_UPDATE : '/updateTour',
        TOUR_UPDATE : '/cricketapi/uploadSportsTours',
        TOUR_LIST_UPLOAD : '/cricketapi/uploadSportsTourMatches',
        
        MATCH_PLAYER_LIST: '/findTourPlayersList',
        PLAYER_INFO : '/findTourPlayersByProperty',
        UPDATE_PLAYER_DATA : '/updateTourPlayers',
        PLAYER_LIST_UPLOAD : '/cricketapi/uploadSportsTourPlayers',
        
        MATCH_INFO : '/findTourMatchesByProperty',
        MATCH_DATA_UPDATE : '/updateTourMatches',
        MATCH_CLOSE_DATA_UPDATE : '/dstributeMatchWinning',
        MATCH_POINTS_UPDATE :'/cricketapi/uploadSportsMatchPoints',
        
        
        LEAGUE_TEMPLATE_LIST : '/findLeagueTemplateList',
        LEAGUE_TEMP_DATA_BY_ID : '/findLeagueTemplateByProperty',
        CREATE_SPECIAL_LEG : '/createMatchSpecialLeague',
        CREATE_DEFAULT_LEAGUE : '/createMatchDeafultLeague',
        
        GAME_SCORE_TEMPLATE_LIST : '/findScoreTemplateList',
        SCORE_TEMP_DATA_BY_ID : '/findScoreTemplateByProperty',
        UPDATE_GAME_SCORE : '/updateScoreTemplate',
        ADD_GAME_SCORE: '/addScoreTemplate',
        
        TEMPLATE_DATA_SAVE: '/addLeagueTemplate',
        TEMPLATE_DATA_UPDATE : '/updateLeagueTemplate',
        
        LEAGUE_CONFIG_LIST : '/findLeagueConfigList',
        LEAGUE_CONFIG_REPORT : '/findLeagueConfigListReport',
        LEAGUE_REPORT_LIST : '/findLeagueList',
        
        TXN_REPORT_LIST : '/payments/txn/history',
        ACCOUNT_TALLY : '/getUserAccountTally',
        RAKE_TALLY : '/getAdminRakeTally',
        GAME_TXN_REPORT : '/findGameTxnList',
        
        FETCH_PLAYER_INFO : '/reports/user' ,
        FETCH_RAKE_REPORT : '/reports/rake',
        FETCH_BONUS_REPORT : '/reports/bonus',
        });


//   UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.TOUR_UPDATE, data, function (response)  { 