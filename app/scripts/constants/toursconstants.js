'use strict';

angular.module('adminFantasyApp')
    .constant('toursconstants', {
   
   LEAGUE_TEMPLATE :[{
          templateId : 100,
          templateName : '1100 Players',         
   },
   {
          templateId : 200,
          templateName : '2200 Players',         
   },
   {
          templateId : 300,
          templateName : '3300 Players',         
   },
   {
          templateId : 400,
          templateName : '4400 Players',         
   },
   {
          templateId : 500,
          templateName : '5500 Players',         
   },
   {
          templateId : 600,
          templateName : '6600 Players',         
   },
   {
          templateId : 700,
          templateName : '7700 Players',         
   },
   ],
   
   LEAGUE_SIZE_AND_COMMISSION : [{
      id : 1,
      maxTeamRegister : 2,
      commissionRate : 15,
   },
   {
      id : 2,
      maxTeamRegister : 3,
      commissionRate : 10,
   },
   {
      id : 3,
      maxTeamRegister : 4,
      commissionRate : 50,
   },
   {
      id : 4,
      maxTeamRegister : 5,
      commissionRate : 17,
   },
   {
      id : 5,
      maxTeamRegister : 6,
      commissionRate : 17,
   },
   {
      id : 6,
      maxTeamRegister : 7,
      commissionRate : 17,
   },
   {
      id : 7,
      maxTeamRegister : 10,
      commissionRate : 17,
   },
   {
      id : 8,
      maxTeamRegister : 20,
      commissionRate : 17,
   },
   {
      id : 9,
      maxTeamRegister : 25,
      commissionRate : 17,
   },
   {
      id : 10,
      maxTeamRegister : 50,
      commissionRate : 17,
   },
   {
      id : 11,
      maxTeamRegister : 100,
      commissionRate : 16,
   },
   {
      id : 12,
      maxTeamRegister : 200,
      commissionRate : 16,
   },
   {
      id : 13,
      maxTeamRegister : 500,
      commissionRate : 16,
   },
   {
      id : 14,
      maxTeamRegister : 1000,
      commissionRate : 15,
   },
   {
      id : 15,
      maxTeamRegister : 2000,
      commissionRate : 15,
   },
   {
      id : 16,
      maxTeamRegister : 5600,
      commissionRate : 15,
   },
   {
      id : 17,
      maxTeamRegister : 6400,
      commissionRate : 15,
   },
   ],
   
   
  
   
   
    ALL_TOURS_LIST :[
           {
                tourId: 1,
                name : "ENG Tour to IND",
                venue: 'Jaipur',                               
                short_name: "IND VS ENG",                
                tour_start_date:"12-12-2017",
                active_date : "12-12-2017",
                game_type : "CRICKET",
                game_format : "ODI",
                total_matches : "5",
                status: "ACTIVE",
            },
            
            {
                tourId: 2,
                name : "ENG Tour to IND",
                venue: 'Jaipur',                               
                short_name: "IND VS ENG",                
                tour_start_date:"12-12-2017",
                active_date : "12-12-2017",
                game_type : "CRICKET",
                game_format : "ODI",
                total_matches : "5",
                status: "ACTIVE",
            },
            
            {
                tourId: 3,
                name : "ENG Tour to IND",
                venue: 'Jaipur',                               
                short_name: "IND VS ENG",                
                tour_start_date:"12-12-2017",
                active_date : "12-12-2017",
                game_type : "CRICKET",
                game_format : "ODI",
                total_matches : "5",
                status: "ACTIVE",
            },
           
            
            {
                tourId: 4,
               name : "ENG Tour to IND",
                venue: 'Jaipur',                               
                short_name: "IND VS ENG",                
                tour_start_date:"12-12-2017",
                active_date : "12-12-2017",
                game_type : "CRICKET",
                game_format : "ODI",
                total_matches : "5",
                status: "ACTIVE",
            },
                   
    ] ,
    
    SQUAD_PLAYER_LIST:[
           {
                tourId: 1,
                playerId : 123,
                player_name: 'MS Dhoni',  
                player_icon : 'player-wk',
                player_info: "Wicket Keeper",                
                team:"INDIA",
                points : "100",
                credit_points : "12",               
            },
            {
                tourId: 2,
                playerId : 124,
                player_name: 'Sachin Tendulkar',    
                player_icon : 'player-batsmen',
                player_info: "Batsmen",                
                team:"INDIA",
                points : "100",
                credit_points : "12",               
            },
            
            {
                tourId:31,
                playerId : 125,
                player_name: 'Saurabh Ganguly', 
                player_icon : 'player-allround',
                player_info: "All Rounder",                
                team:"INDIA",
                points : "100",
                credit_points : "12",               
            },
            
            {
                tourId: 4,
                playerId : 126,
                player_name: 'Rahul Dravid', 
                player_icon : 'player-batsmen',
                player_info: "Batsmen",                
                team:"INDIA",
                points : "100",
                credit_points : "12",               
            },
            
            {
                tourId: 5,
                playerId : 127,
                player_name: 'Virendrer Sehwag',   
                player_icon : 'player-batsmen',
                player_info: "Batsmen",                
                team:"INDIA",
                points : "100",
                credit_points : "12",               
            },
            {
                tourId: 6,
                playerId : 128,
                player_name: 'Zaheer Khan',    
                player_icon : 'player-bowler',
                player_info: "Bowler",                
                team:"INDIA",
                points : "100",
                credit_points : "12",               
            },
    ],
    
    ALL_TOUR_LIST : [{
            id: 1234,
            tourName : "End Tour To India",          
    },
    {
            id: 1234,
            tourName : "End Tour To India",          
    },
    {
            id: 1234,
            tourName : "End Tour To India",          
    },
    {
            id: 1234,
            tourName : "End Tour To India",          
    },
],
    
    
    
    
    
    TOUR_MATCHES : [
        {
            id:123,
            name : 'Indis England Onday Series',
            relatedName : 'Ind vs Eng',
            startTime : '12-1-2016',
            teamA : {
                name : 'India',
            },
            teamB : {
                name : 'England',
            },
            status : "Active",
            guruLink : 'end-vs-eng-1st_odi',
        },
        {
            id:123,
            name : 'Indis England Onday Series',
            relatedName : 'Ind vs Eng',
            startTime : '12-1-2016',
            teamA : {
                name : 'India',
            },
            teamB : {
                name : 'England',
            },
            status : "Active",
            guruLink : 'end-vs-eng-1st_odi',
        },
        {
            id:123,
            name : 'Indis England Onday Series',
            relatedName : 'Ind vs Eng',
            startTime : '12-1-2016',
            teamA : {
                name : 'India',
            },
            teamB : {
                name : 'England',
            },
            status : "Active",
            guruLink : 'end-vs-eng-1st_odi',
        }
    ]
    
    
 
});

