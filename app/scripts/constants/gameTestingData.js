'use strict';
angular.module('adminFantasyApp')
        .constant('gameJsonData', {
            T20Match: {
                "status": true,
                "status_code": 200,
                "data": {
                    "card_type": "full_card",
                    "card": {
                        "related_name": "1st T20 Match",
                        "key": "vw_t20",
                        "status": "completed",
                        "short_name": "Vampires Vs Wolves T20",
                        "format": "t20",
                        "status_overview": "result",
                        "match_overs": 20,
                        "teams": {
                            "a": {
                                "key": "X",
                                "short_name": "X",
                                "match": {
                                    "playing_xi": [
                                        "btx1",
                                        "btx2",
                                        "btx3",
                                        "btx4",
                                        "bwx1",
                                        "bwx2",
                                        "bwx3",
                                        "bwx4",
                                        "arx1",
                                        "arx2",
                                        "wkx1"
                                    ],
                                    "key": "X",
                                    "keeper": "wkx1",
                                    "season_team_key": "X",
                                    "captain": "btx1"
                                },
                                "name": "X"
                            },
                            "b": {
                                "key": "Y",
                                "short_name": "Y",
                                "match": {
                                    "playing_xi": [
                                        "bty1",
                                        "bty2",
                                        "bty3",
                                        "bty4",
                                        "bwy1",
                                        "bwy2",
                                        "bwy3",
                                        "bwy4",
                                        "ary1",
                                        "ary2",
                                        "wky1"
                                    ],
                                    "key": "X",
                                    "keeper": "wky1",
                                    "season_team_key": "Y",
                                    "captain": "bty1"
                                },
                                "name": "Y"
                            }
                        },
                        "players" : {
                            "btx1": {
                                "fullname": "btx1",
                                "name": "btx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 10,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx2": {
                                "fullname": "btx2",
                                "name": "btx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx3": {
                                "fullname": "btx3",
                                "name": "btx3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx4": {
                                "fullname": "btx4",
                                "name": "btx4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx1": {
                                "fullname": "bwx1",
                                "name": "bwx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx2": {
                                "fullname": "bwx2",
                                "name": "bwx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx3": {
                                "fullname": "bwx3",
                                "name": "bwx3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx4": {
                                "fullname": "bwx4",
                                "name": "bwx4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "arx1": {
                                "fullname": "arx1",
                                "name": "arx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "arx2": {
                                "fullname": "arx2",
                                "name": "arx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "wkx1": {
                                "fullname": "wkx1",
                                "name": "wkx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty1": {
                                "fullname": "bty1",
                                "name": "bty1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty2": {
                                "fullname": "bty2",
                                "name": "bty2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty3": {
                                "fullname": "bty3",
                                "name": "bty3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty4": {
                                "fullname": "bty4",
                                "name": "bty4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy1": {
                                "fullname": "bwy1",
                                "name": "bwy1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy2": {
                                "fullname": "bwy2",
                                "name": "bwy2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy3": {
                                "fullname": "bwy3",
                                "name": "bwy3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy4": {
                                "fullname": "bwy4",
                                "name": "bwy4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "ary1": {
                                "fullname": "ary1",
                                "name": "ary1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "ary2": {
                                "fullname": "ary2",
                                "name": "ary2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "wky1": {
                                "fullname": "wky1",
                                "name": "wky1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "data_review_checkpoint": "post-match-validated"
                    }
                }
            },
            TestMatch: {
                "status": true,
                "status_code": 200,
                "data": {
                    "card_type": "full_card",
                    "card": {
                        "related_name": "1st TEST Match",
                        "key": "vw_test",
                        "status": "completed",
                        "short_name": "Vampires Vs Wolves T20",
                        "format": "test",
                        "status_overview": "result",
                        "match_overs": 20,
                        "teams": {
                            "a": {
                                "key": "X",
                                "short_name": "X",
                                "match": {
                                    "playing_xi": [
                                        "btx1",
                                        "btx2",
                                        "btx3",
                                        "btx4",
                                        "bwx1",
                                        "bwx2",
                                        "bwx3",
                                        "bwx4",
                                        "arx1",
                                        "arx2",
                                        "wkx1"
                                    ],
                                    "key": "X",
                                    "keeper": "wkx1",
                                    "season_team_key": "X",
                                    "captain": "btx1"
                                },
                                "name": "X"
                            },
                            "b": {
                                "key": "Y",
                                "short_name": "Y",
                                "match": {
                                    "playing_xi": [
                                        "bty1",
                                        "bty2",
                                        "bty3",
                                        "bty4",
                                        "bwy1",
                                        "bwy2",
                                        "bwy3",
                                        "bwy4",
                                        "ary1",
                                        "ary2",
                                        "wky1"
                                    ],
                                    "key": "X",
                                    "keeper": "wky1",
                                    "season_team_key": "Y",
                                    "captain": "bty1"
                                },
                                "name": "Y"
                            }
                        },
                        "players" : {
                            "btx1": {
                                "fullname": "btx1",
                                "name": "btx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 10,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx2": {
                                "fullname": "btx2",
                                "name": "btx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx3": {
                                "fullname": "btx3",
                                "name": "btx3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx4": {
                                "fullname": "btx4",
                                "name": "btx4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx1": {
                                "fullname": "bwx1",
                                "name": "bwx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx2": {
                                "fullname": "bwx2",
                                "name": "bwx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx3": {
                                "fullname": "bwx3",
                                "name": "bwx3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx4": {
                                "fullname": "bwx4",
                                "name": "bwx4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "arx1": {
                                "fullname": "arx1",
                                "name": "arx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "arx2": {
                                "fullname": "arx2",
                                "name": "arx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "wkx1": {
                                "fullname": "wkx1",
                                "name": "wkx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty1": {
                                "fullname": "bty1",
                                "name": "bty1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty2": {
                                "fullname": "bty2",
                                "name": "bty2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty3": {
                                "fullname": "bty3",
                                "name": "bty3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty4": {
                                "fullname": "bty4",
                                "name": "bty4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy1": {
                                "fullname": "bwy1",
                                "name": "bwy1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy2": {
                                "fullname": "bwy2",
                                "name": "bwy2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy3": {
                                "fullname": "bwy3",
                                "name": "bwy3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy4": {
                                "fullname": "bwy4",
                                "name": "bwy4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "ary1": {
                                "fullname": "ary1",
                                "name": "ary1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "ary2": {
                                "fullname": "ary2",
                                "name": "ary2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "wky1": {
                                "fullname": "wky1",
                                "name": "wky1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "data_review_checkpoint": "post-match-validated"
                    }
                }
            },
            ODIMatch: {
                "status": true,
                "status_code": 200,
                "data": {
                    "card_type": "full_card",
                    "card": {
                        "related_name": "1st ODI Match",
                        "key": "vw_odi",
                        "status": "completed",
                        "short_name": "Vampires Vs Wolves odi",
                        "format": "ONE_DAY",
                        "status_overview": "result",
                        "match_overs": 20,
                        "teams": {
                            "a": {
                                "key": "X",
                                "short_name": "X",
                                "match": {
                                    "playing_xi": [
                                        "btx1",
                                        "btx2",
                                        "btx3",
                                        "btx4",
                                        "bwx1",
                                        "bwx2",
                                        "bwx3",
                                        "bwx4",
                                        "arx1",
                                        "arx2",
                                        "wkx1"
                                    ],
                                    "key": "X",
                                    "keeper": "wkx1",
                                    "season_team_key": "X",
                                    "captain": "btx1"
                                },
                                "name": "X"
                            },
                            "b": {
                                "key": "Y",
                                "short_name": "Y",
                                "match": {
                                    "playing_xi": [
                                        "bty1",
                                        "bty2",
                                        "bty3",
                                        "bty4",
                                        "bwy1",
                                        "bwy2",
                                        "bwy3",
                                        "bwy4",
                                        "ary1",
                                        "ary2",
                                        "wky1"
                                    ],
                                    "key": "X",
                                    "keeper": "wky1",
                                    "season_team_key": "Y",
                                    "captain": "bty1"
                                },
                                "name": "Y"
                            }
                        },
                        "players" : {
                            "btx1": {
                                "fullname": "btx1",
                                "name": "btx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 10,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx2": {
                                "fullname": "btx2",
                                "name": "btx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx3": {
                                "fullname": "btx3",
                                "name": "btx3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "btx4": {
                                "fullname": "btx4",
                                "name": "btx4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx1": {
                                "fullname": "bwx1",
                                "name": "bwx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx2": {
                                "fullname": "bwx2",
                                "name": "bwx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx3": {
                                "fullname": "bwx3",
                                "name": "bwx3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwx4": {
                                "fullname": "bwx4",
                                "name": "bwx4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "arx1": {
                                "fullname": "arx1",
                                "name": "arx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "arx2": {
                                "fullname": "arx2",
                                "name": "arx2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "wkx1": {
                                "fullname": "wkx1",
                                "name": "wkx1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty1": {
                                "fullname": "bty1",
                                "name": "bty1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty2": {
                                "fullname": "bty2",
                                "name": "bty2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty3": {
                                "fullname": "bty3",
                                "name": "bty3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bty4": {
                                "fullname": "bty4",
                                "name": "bty4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy1": {
                                "fullname": "bwy1",
                                "name": "bwy1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy2": {
                                "fullname": "bwy2",
                                "name": "bwy2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy3": {
                                "fullname": "bwy3",
                                "name": "bwy3",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "bwy4": {
                                "fullname": "bwy4",
                                "name": "bwy4",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "ary1": {
                                "fullname": "ary1",
                                "name": "ary1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "ary2": {
                                "fullname": "ary2",
                                "name": "ary2",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            },
                            "wky1": {
                                "fullname": "wky1",
                                "name": "wky1",
                                "match": {
                                    "innings": {
                                        "1": {
                                            "fielding": {
                                                "catches": 0,
                                                "runouts": 0,
                                                "stumbeds": 0
                                            },
                                            "batting": {
                                                "dots": 0,
                                                "sixes": 0,
                                                "runs": 0,
                                                "balls": 0,
                                                "fours": 0,
                                                "strike_rate": 0,
                                                "dismissed": false
                                            },
                                            "bowling": {
                                                "dots": 0,
                                                "maiden_overs": 0,
                                                "wickets": 0,
                                                "overs": 0,
                                                "economy": 0
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "data_review_checkpoint": "post-match-validated"
                    }
                }
            },

        matchTypeList: [{
            gameFormat: "T20"
        }, {
            gameFormat: "TEST"
        },{
            gameFormat: "ODI"
        },
        ]
    });









//
//var T20game = {
//    "status": true,
//    "status_code": 200,
//    "data": {
//        "card_type": "full_card",
//        "card": {
//            "related_name": "1st T20 Match",
//            "key": "vw_t20",
//            "status": "completed",
//            "short_name": "Vampires Vs Wolves T20",
//            "format": "t20",
//            "status_overview": "result",
//            "match_overs": 20,
//            "teams": {
//                "a": {
//                    "key": "X",
//                    "short_name": "X",
//                    "match": {
//                        "playing_xi": [
//                            "btx1",
//                            "btx2",
//                            "btx3",
//                            "btx4",
//                            "bwx1",
//                            "bwx2",
//                            "bwx3",
//                            "bwx4",
//                            "arx1",
//                            "arx2",
//                            "wkx1"
//                        ],
//                        "key": "X",
//                        "keeper": "wkx1",
//                        "season_team_key": "X",
//                        "captain": "btx1"
//                    },
//                    "name": "X"
//                },
//                "b": {
//                    "key": "Y",
//                    "short_name": "Y",
//                    "match": {
//                        "playing_xi": [
//                            "bty1",
//                            "bty2",
//                            "bty3",
//                            "bty4",
//                            "bwy1",
//                            "bwy2",
//                            "bwy3",
//                            "bwy4",
//                            "ary1",
//                            "ary2",
//                            "wky1"
//                        ],
//                        "key": "X",
//                        "keeper": "wky1",
//                        "season_team_key": "Y",
//                        "captain": "bty1"
//                    },
//                    "name": "Y"
//                }
//            },
//            
//            "data_review_checkpoint": "post-match-validated"
//        }
//    }
//};
//
//var ODIgame = {
//    "status": true,
//    "status_code": 200,
//    "data": {
//        "card_type": "full_card",
//        "card": {
//            "related_name": "1st ODI Match",
//            "key": "vw_odi",
//            "status": "completed",
//            "short_name": "Vampires Vs Wolves ODI",
//            "format": "odi",
//            "status_overview": "result",
//            "match_overs": 20,
//            "teams": {
//                "a": {
//                    "key": "X",
//                    "short_name": "X",
//                    "match": {
//                        "playing_xi": [
//                            "btx1",
//                            "btx2",
//                            "btx3",
//                            "btx4",
//                            "bwx1",
//                            "bwx2",
//                            "bwx3",
//                            "bwx4",
//                            "arx1",
//                            "arx2",
//                            "wkx1"
//                        ],
//                        "key": "X",
//                        "keeper": "wkx1",
//                        "season_team_key": "X",
//                        "captain": "btx1"
//                    },
//                    "name": "X"
//                },
//                "b": {
//                    "key": "Y",
//                    "short_name": "Y",
//                    "match": {
//                        "playing_xi": [
//                            "bty1",
//                            "bty2",
//                            "bty3",
//                            "bty4",
//                            "bwy1",
//                            "bwy2",
//                            "bwy3",
//                            "bwy4",
//                            "ary1",
//                            "ary2",
//                            "wky1"
//                        ],
//                        "key": "X",
//                        "keeper": "wky1",
//                        "season_team_key": "Y",
//                        "captain": "bty1"
//                    },
//                    "name": "Y"
//                }
//            },
//            
//            "data_review_checkpoint": "post-match-validated"
//        }
//    }
//};
//
//var Testgame = {
//    "status": true,
//    "status_code": 200,
//    "data": {
//        "card_type": "full_card",
//        "card": {
//            "related_name": "1st Test Match",
//            "key": "vw_test",
//            "status": "completed",
//            "short_name": "Vampires Vs Wolves T20",
//            "format": "test",
//            "status_overview": "result",
//            "match_overs": 20,
//            "teams": {
//                "a": {
//                    "key": "X",
//                    "short_name": "X",
//                    "match": {
//                        "playing_xi": [
//                            "btx1",
//                            "btx2",
//                            "btx3",
//                            "btx4",
//                            "bwx1",
//                            "bwx2",
//                            "bwx3",
//                            "bwx4",
//                            "arx1",
//                            "arx2",
//                            "wkx1"
//                        ],
//                        "key": "X",
//                        "keeper": "wkx1",
//                        "season_team_key": "X",
//                        "captain": "btx1"
//                    },
//                    "name": "X"
//                },
//                "b": {
//                    "key": "Y",
//                    "short_name": "Y",
//                    "match": {
//                        "playing_xi": [
//                            "bty1",
//                            "bty2",
//                            "bty3",
//                            "bty4",
//                            "bwy1",
//                            "bwy2",
//                            "bwy3",
//                            "bwy4",
//                            "ary1",
//                            "ary2",
//                            "wky1"
//                        ],
//                        "key": "X",
//                        "keeper": "wky1",
//                        "season_team_key": "Y",
//                        "captain": "bty1"
//                    },
//                    "name": "Y"
//                }
//            },
//            
//            "data_review_checkpoint": "post-match-validated"
//        }
//    }
//}
//
//game.players = {
//    "btx1": {
//        "fullname": "btx1",
//        "name": "btx1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "btx2": {
//        "fullname": "btx2",
//        "name": "btx2",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "btx3": {
//        "fullname": "btx3",
//        "name": "btx3",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "btx4": {
//        "fullname": "btx4",
//        "name": "btx4",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwx1": {
//        "fullname": "bwx1",
//        "name": "bwx1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwx2": {
//        "fullname": "bwx2",
//        "name": "bwx2",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwx3": {
//        "fullname": "bwx3",
//        "name": "bwx3",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwx4": {
//        "fullname": "bwx4",
//        "name": "bwx4",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "arx1": {
//        "fullname": "arx1",
//        "name": "arx1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "arx2": {
//        "fullname": "arx2",
//        "name": "arx2",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "wkx1": {
//        "fullname": "wkx1",
//        "name": "wkx1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//     "bty1": {
//        "fullname": "bty1",
//        "name": "bty1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bty2": {
//        "fullname": "bty2",
//        "name": "bty2",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bty3": {
//        "fullname": "bty3",
//        "name": "bty3",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bty4": {
//        "fullname": "bty4",
//        "name": "bty4",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwy1": {
//        "fullname": "bwy1",
//        "name": "bwy1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwy2": {
//        "fullname": "bwy2",
//        "name": "bwy2",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwy3": {
//        "fullname": "bwy3",
//        "name": "bwy3",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "bwy4": {
//        "fullname": "bwy4",
//        "name": "bwy4",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "ary1": {
//        "fullname": "ary1",
//        "name": "ary1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "ary2": {
//        "fullname": "ary2",
//        "name": "ary2",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    },
//    "wky1": {
//        "fullname": "wky1",
//        "name": "wky1",
//        "match": {
//            "innings": {
//                "1": {
//                    "fielding": {
//                        "catches": 0,
//                        "runouts": 0,
//                        "stumbeds": 0
//                    },
//                    "batting": {
//                        "dots": 0,
//                        "sixes": 0,
//                        "runs": 0,
//                        "balls": 0,
//                        "fours": 0,
//                        "strike_rate": 0,
//                        "dismissed": false
//                    },
//                    "bowling": {
//                        "dots": 0,
//                        "maiden_overs": 0,
//                        "wickets": 0,
//                        "overs": 0,
//                        "economy": 0
//                    }
//                }
//            }
//        }
//    }
//};
//                   
