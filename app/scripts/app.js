'use strict';

function menuToggle(){
  // console.log('test menu slide');
  // $('.page-wrapper').toggleClass('menu-open');
  // if($('.main-content').hasClass('no-transition'))
  //   $('.main-content').removeClass('no-transition');
}


/**
 * @ngdoc overview
 * @name adminFantasyApp
 * @description
 * # adminFantasyApp
 *
 * Main module of the application.
 */
angular
  .module('adminFantasyApp', [

    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap.datetimepicker',
    'ui.dateTimeInput',
    'ngOnlyNumberApp',
    'ngJsonExplorer',
    'ngDialog',
    
    'ui.select',
    'ngFileUpload',
    'ui.grid',
    'truncate'

  ]) .run(function($rootScope, $timeout,$location, $state, UtilityService, myConfig, serverApi) {
    $rootScope.loginSuccess =false;

    // To validate if user is logged in otherwise redirect to login page
    $rootScope.$on("$stateChangeStart", function() {
        var data = {};
        data.token = localStorage.adminToken;
        if (data.token && data.token != '') {
             UtilityService._postAjaxCall(myConfig.websiteUrl + serverApi.VALIDATE_TOKEN, data, function (tokenResponseData) {
                 var respData = tokenResponseData.respData;
                if (tokenResponseData.respCode == 100) {
                    $rootScope.userId = respData.userId;
                    $rootScope.loginSuccess = true;
                }
                else if(tokenResponseData.respCode == 108){
                    $rootScope.loginSuccess = true;
                    $rootScope.userId = respData.userId;
                }
                else {
                    $rootScope.loginSuccess = false;
                    $location.path('login');
                }
            });

        }
        
//      if(!$rootScope.loginSuccess){
//        $timeout(function(){
//          $state.go('login');
//        },10);
//      }
    });
} )

  .config(["$stateProvider","$urlRouterProvider",function ($stateProvider, $urlRouterProvider, $rootScope) {
    // console.log("$rootScope.defaultUrl",$rootScope.defaultUrl);
     $urlRouterProvider.otherwise('dashboard');
     $stateProvider
      .state('login', {
            url: "/login",
            data: {title: "LoginCtrl"},
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
      })
      .state('dashboard', {
            url: "/dashboard",
            data: {title: "dashboard"},
            templateUrl: 'views/dashboard.html',
      })
      .state('creditToUser', {
            url: "/creditToUser",
            data: {title: "creditToUser"},
            templateUrl: 'views/crm/creditToUser.html',
            controller: 'CreditToUserCtrl',
            controllerAs: 'credit',
      })
      .state('debitFromUser', {
            url: "/debitFromUser",
            data: {title: "debitFromUser"},
            templateUrl: 'views/crm/debitFromUser.html',
            controller: 'DebitFromUserCtrl',
            controllerAs: 'debit'
      })     
      .state('resetPassword', {
            url: "/resetPassword",
            data: {title: "resetPassword"},
            templateUrl: 'views/crm/resetPassword.html',
            controller: 'resetPasswordCtrl',
            controllerAs: 'resetpassword'
      })
      .state('changePassword', {
            url: "/changePassword",
            data: {title: "changePassword"},
            templateUrl: 'views/crm/changePassword.html',
            controller: 'changePasswordCtrl',
            controllerAs: 'changepassword'
      })     
      .state('playerDetail', {
            url: "/playerDetail",
            data: {title: "playerDetail"},
            templateUrl: 'views/crm/playerDetail.html',
            controller: 'playerDetailCtrl',
            controllerAs: 'playerdetail'
      })
      .state('editor', {
            url: "/editor",
            data: {title: "editor"},
            templateUrl: 'views/editor.html',
            controller: 'editorCtrl',
            controllerAs: 'editor'
      })     
     .state('emailEditor', {
            url: "/emailEditor",
            data: {title: "emailEditor"},
            templateUrl: 'views/emailEditor.html',
            controller: 'emailEditorCtrl',
            controllerAs: 'email'
      })
      .state('allTours', {
            url: "/allTourList",
            data: {title: "allToursList"},
            templateUrl: 'views/tours/allToursList.html',
            controller: 'allToursListCtrl',
            controllerAs: 'allTours'
      })
      .state('squadPlayer', {
            url: "/squadPlayer",
            data: {title: "squadPlayer"},
            templateUrl: 'views/tours/squadPlayers.html',
            controller: 'squadPlayerCtrl',
            controllerAs: 'squadplayer'
      })
      .state('leagueTemplate', {
            url: "/leagueTemplate",
            data: {title: "leagueTemplate"},
            templateUrl: 'views/league/leagueTemplate.html',
            controller: 'leagueTemplateCtrl',
            controllerAs: 'leagueTemplate'
      })   
      .state('tourMatches', {
            url: "/tourMatches",
            data: {title: "tourMatches"},
            templateUrl: 'views/tours/tourMatches.html',
            controller: 'tourMatchesCtrl',
            controllerAs: 'tourMatches'
      })
      .state('wicketDetails', {
            url: "/wicketDetails",
            data: {title: "wicketDetails"},
            templateUrl: 'views/match/wicketDetails.html',
            controller: 'wicketDetailsCtrl',
            controllerAs: 'wicketDetails'
      })
      .state('cricketApi', {
            url: "/cricketApi",
            data: {title: "cricketApi"},
            templateUrl: 'views/cricketApi.html',
            controller: 'cricketApiCtrl',
            controllerAs: 'cricketApi'
      })
      .state('createLeague', {
            url: "/createLeague",
            data: {title: "createLeague"},
            templateUrl: 'views/league/createLeague.html',
            controller: 'createLeagueCtrl',
            controllerAs: 'createLeague'
      })
      .state('pancardVerify', {
            url: "/pancardVerify",
            data: {title: "pancardVerify"},
            templateUrl: 'views/crm/pancardVerify.html',
            controller: 'pancardVerifyCtrl',
            controllerAs: 'pancardVerify'
      })
      .state('docVerification', {
            url: "/docVerification",
            data: {title: "docVerification"},
            templateUrl: 'views/crm/docVerification.html',
            controller: 'docVerifyCtrl',
            controllerAs: 'docVerify'
      })
      .state('adminCompliance', {
            url: "/adminCompliance",
            data: {title: "adminCompliance"},
            templateUrl: 'views/crm/adminCompliance.html',
            controller: 'adminComplianceCtrl',
            controllerAs: 'adminCompliance'
      })
      .state('withdrawalApproveAccounts', {
            url: "/withdrawalApproveAccounts",
            data: {title: "withdrawalApproveAccounts"},
            templateUrl: 'views/withdrawalApprove.html',
            controller: 'withdrawalApproveCtrl',
            controllerAs: 'withdrawalApprove'
      })
      .state('userReports', {
            url: "/userReports",
            data: {title: "userReports"},
            templateUrl: 'views/reports/userReports.html',
            controller: 'UserReportsCtrl',
            controllerAs: 'userReports'
      })
      .state('leagueReports', {
            url: "/leagueReports",
            data: {title: "reports"},
            templateUrl: 'views/reports/leagueReports.html',
            controller: 'LeagueReportsCtrl',
            controllerAs: 'leagueReports'
      })  
      .state('userTxnReports', {
            url: "/userTxnReports",
            data: {title: "reports"},
            templateUrl: 'views/reports/userTxnReports.html',
            controller: 'UserTxnReportsCtrl',
            controllerAs: 'txnReports'
      })
      .state('gameTesting', {
            url: "/gameTesting",
            data: {title: "gameTesting"},
            templateUrl: 'views/gameTesting.html',
            controller: 'gameTestingCtrl',
            controllerAs: 'gameTesting'
      })
      .state('tallyReport', {
            url: "/tallyReport",
            data: {title: "tallyReport"},
            templateUrl: 'views/reports/tallyReport.html',
            controller: 'tallyReportCtrl',
            controllerAs: 'tallyReport'
      })
      .state('rakeReport', {
            url: "/rakeReport",
            data: {title: "rakeReport"},
            templateUrl: 'views/reports/rakeReport.html',
            controller: 'rakeReportCtrl',
            controllerAs: 'rakeReport'
      })
      .state('collectionReport', {
            url: "/collectionReport",
            data: {title: "collectionReport"},
            templateUrl: 'views/reports/userCollectionReportCtrl.html',
            controller: 'userCollectionReportCtrl',
            controllerAs: 'collectionReport'
      })
      .state('bonusReport', {
            url: "/bonusReport",
            data: {title: "bonusReport"},
            templateUrl: 'views/reports/bonusReport.html',
            controller: 'bonusReportCtrl',
            controllerAs: 'bonusReport'
      });

    }]);
